#ifndef SHA512_H
#define SHA512_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cuda.h>
#include <inttypes.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#define DEVICE 0


__constant__ uint32_t	h0_c[16] = {
	0xf3bcc908, 0x6a09e667,
	0x84caa73b, 0xbb67ae85,
	0xfe94f82b, 0x3c6ef372,
	0x5f1d36f1, 0xa54ff53a,
	0xade682d1, 0x510e527f,
	0x2b3e6c1f, 0x9b05688c,
	0xfb41bd6b, 0x1f83d9ab,
	0x137e2179, 0x5be0cd19 };

__constant__ uint32_t k[160] = {
	0xD728AE22, 0x428A2F98,			0x23EF65CD, 0x71374491,
	0xEC4D3B2F, 0xB5C0FBCF,			0x8189DBBC, 0xE9B5DBA5,
	0xF348B538, 0x3956C25B,			0xB605D019, 0x59F111F1,
	0xAF194F9B, 0x923F82A4,			0xDA6D8118, 0xAB1C5ED5,
	0xA3030242, 0xD807AA98,			0x45706FBE, 0x12835B01,
	0x4EE4B28C, 0x243185BE,			0xD5FFB4E2, 0x550C7DC3,
	0xF27B896F, 0x72BE5D74,			0x3B1696B1, 0x80DEB1FE,
	0x25C71235, 0x9BDC06A7,			0xCF692694, 0xC19BF174,
	0x9EF14AD2, 0xE49B69C1,			0x384F25E3, 0xEFBE4786,
	0x8B8CD5B5, 0x0FC19DC6,			0x77AC9C65, 0x240CA1CC,
	0x592B0275, 0x2DE92C6F,			0x6EA6E483, 0x4A7484AA,
	0xBD41FBD4, 0x5CB0A9DC,			0x831153B5, 0x76F988DA,
	0xEE66DFAB, 0x983E5152,			0x2DB43210, 0xA831C66D,
	0x98FB213F, 0xB00327C8,			0xBEEF0EE4, 0xBF597FC7,
	0x3DA88FC2, 0xC6E00BF3,			0x930AA725, 0xD5A79147,
	0xE003826F, 0x06CA6351,			0x0A0E6E70, 0x14292967,
	0x46D22FFC, 0x27B70A85,			0x5C26C926, 0x2E1B2138,
	0x5AC42AED, 0x4D2C6DFC,			0x9D95B3DF, 0x53380D13,
	0x8BAF63DE, 0x650A7354,			0x3C77B2A8, 0x766A0ABB,
	0x47EDAEE6, 0x81C2C92E,			0x1482353B, 0x92722C85,
	0x4CF10364, 0xA2BFE8A1,			0xBC423001, 0xA81A664B,
	0xD0F89791, 0xC24B8B70,			0x0654BE30, 0xC76C51A3,
	0xD6EF5218, 0xD192E819,			0x5565A910, 0xD6990624,
	0x5771202A, 0xF40E3585,			0x32BBD1B8, 0x106AA070,
	0xB8D2D0C8, 0x19A4C116,			0x5141AB53, 0x1E376C08,
	0xDF8EEB99, 0x2748774C,			0xE19B48A8, 0x34B0BCB5,
	0xC5C95A63, 0x391C0CB3,			0xE3418ACB, 0x4ED8AA4A,
	0x7763E373, 0x5B9CCA4F,			0xD6B2B8A3, 0x682E6FF3,
	0x5DEFB2FC, 0x748F82EE,			0x43172F60, 0x78A5636F,
	0xA1F0AB72, 0x84C87814,			0x1A6439EC, 0x8CC70208,
	0x23631E28, 0x90BEFFFA,			0xDE82BDE9, 0xA4506CEB,
	0xB2C67915, 0xBEF9A3F7,			0xE372532B, 0xC67178F2,
	0xEA26619C, 0xCA273ECE,			0x21C0C207, 0xD186B8C7,
	0xCDE0EB1E, 0xEADA7DD6,			0xEE6ED178, 0xF57D4F7F,
	0x72176FBA, 0x06F067AA,			0xA2C898A6, 0x0A637DC5,
	0xBEF90DAE, 0x113F9804,			0x131C471B, 0x1B710B35,
	0x23047D84, 0x28DB77F5,			0x40C72493, 0x32CAAB7B,
	0x15C9BEBC, 0x3C9EBE0A,			0x9C100D4C, 0x431D67C4,
	0xCB3E42B6, 0x4CC5D4BE,			0xFC657E2A, 0x597F299C,
	0x3AD6FAEC, 0x5FCB6FAB,			0x4A475817, 0x6C44198C };




#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 320
#define SHLA(x0, x1, n) ((x0 << n))				
#define SHLB(x0, x1, n) __funnelshift_l(x0, x1, n)
#define SHRA(x0, x1, n) __funnelshift_r(x0, x1, n)
#define SHRB(x0, x1, n) ((x1 >> n))
#else
#define SHLA(x0, x1, n) ((x0 << n))						
#define SHLB(x0, x1, n) ((x1 << n)| (x0 >> (32 - n)))	
#define SHRA(x0, x1, n) ((x0 >> n)| (x1 << (32 - n))) 
#define SHRB(x0, x1, n) ((x1 >> n))
#endif

#define ROTRA(x0, x1, n) (SHRA(x0, x1, n))
#define ROTRB(x0, x1, n) (SHRB(x0, x1, n)| (x0 << (32 - n)) )  

#define ROTLA(x0, x1, n) (SHLA(x0, x1, n)| (x1 >> (32 - n)) )
#define ROTLB(x0, x1, n) (SHLB(x0, x1, n))

#define S0A(x0, x1) (ROTRA(x0, x1, 1)^(ROTRA(x0, x1, 8))^(SHRA(x0, x1, 7)))
#define S0B(x0, x1) (ROTRB(x0, x1, 1)^(ROTRB(x0, x1, 8))^(SHRB(x0, x1, 7)))

#define S1A(x0, x1) (ROTRA(x0, x1, 19)^(ROTLA(x0, x1, 3))^(SHRA(x0, x1, 6)))
#define S1B(x0, x1) (ROTRB(x0, x1, 19)^(ROTLB(x0, x1, 3))^(SHRB(x0, x1, 6)))

#define S2A(x0, x1) (ROTRA(x0, x1, 28)^(ROTLA(x0, x1, 30))^(ROTLA(x0, x1, 25)))
#define S2B(x0, x1) (ROTRB(x0, x1, 28)^(ROTLB(x0, x1, 30))^(ROTLB(x0, x1, 25)))

#define S3A(x0, x1) (ROTRA(x0, x1, 14)^(ROTRA(x0, x1, 18))^(ROTLA(x0, x1, 23)))
#define S3B(x0, x1) (ROTRB(x0, x1, 14)^(ROTRB(x0, x1, 18))^(ROTLB(x0, x1, 23)))

#define F0(x,y,z) ((x & y) | (z & (x | y)))
#define F1(x,y,z) (z ^ (x & (y ^ z)))
__forceinline__ __device__ uint32_t thread_dependB()
{
	int y = blockIdx.x / 2;
	uint32_t ret = 0;

	unsigned char a;
	y = y % 100;

	a = y / 10;
	a = a + 0x30;
	ret += (a & 0x000000ff) << 24;
	y = y % 10;

	a = y;
	a = a + 0x30;
	ret += (a & 0x000000ff) << 16;
	return ret;
}

__forceinline__ __device__ uint32_t thread_dependA()
{
	int y = blockIdx.x / 2;
	int x = threadIdx.x + 500 * (blockIdx.x % 2);

	uint32_t ret = 0;

	unsigned char a = x / 100;
	a = a + 0x30;
	ret += (a & 0x000000ff) << 24;
	x = x % 100;

	a = x / 10;
	a = a + 0x30;
	ret += (a & 0x000000ff) << 16;
	x = x % 10;

	a = x;
	a = a + 0x30;
	ret += (a & 0x000000ff) << 8;

	a = y / 100;
	a = a + 0x30;
	ret += (a & 0x000000ff);
	y = y % 100;

	return ret;
}


__forceinline__ __device__ int SUMA(int a, int b) {
	int c;
	asm("add.cc.u32      %0, %1, %2;"
		: "=r"(c)
		: "r"(a), "r"(b));
	return c;
}

__forceinline__ __device__ int SUMB(int a, int b) {
	int c;
	asm("addc.u32      %0, %1, %2;"
		: "=r"(c)
		: "r"(a), "r"(b));
	return c;
}

#define SUM(x0, x1, y0, y1, z0, z1)	{			\
	asm (	"add.cc.u32      %0, %2, %4;\n\t"	\
			"addc.u32        %1, %3, %5;"		\
				: "=r"(z0), "=r"(z1)			\
				:  "r"(x0),  "r"(x1),			\
				   "r"(y0),  "r"(y1));			\
}

__device__ void sha512(uint32_t * words) {

	uint32_t temp1A, temp1B, temp2A, temp2B, tsum1a, tsum1b, tsum2a, tsum2b, z1a, z2a, z3a, z1b, z2b, z3b;

#define P(aA, aB, bA, bB, cA, cB, dA, dB, eA, eB, fA, fB, gA, gB, hA, hB, xA, xB, KA, KB)					\
	{																											\
		SUM (hA, hB, S3A(eA, eB), S3B(eA, eB), z1a, z1b );														\
		SUM (F1(eA,fA,gA), F1(eB,fB,gB), KA, KB, z2a, z2b );													\
		SUM (z1a, z1b, z2a, z2b, z3a, z3b );																	\
		SUM (z3a, z3b, xA, xB, temp1A, temp1B);																	\
		SUM (S2A(aA, aB), S2B(aA, aB), F0(aA,bA,cA), F0(aB,bB,cB), temp2A, temp2B );							\
		SUM (dA, dB, temp1A, temp1B, dA, dB );																	\
		SUM (temp1A, temp1B, temp2A, temp2B, hA, hB);															\
	}

#define I(a0, a1, b0, b1, c0, c1, d0, d1, w0, w1)				\
	{																\
	SUM(S1A(a0, a1), S1B(a0, a1), b0, b1, tsum1a, tsum1b);			\
	SUM(S0A(c0, c1), S0B(c0, c1), d0, d1, tsum2a, tsum2b);			\
	SUM(tsum1a, tsum1b, tsum2a, tsum2b, w0, w1);					\
	}


	uint32_t Aa = h0_c[0];
	uint32_t Ab = h0_c[1];
	uint32_t Ba = h0_c[2];
	uint32_t Bb = h0_c[3];
	uint32_t Ca = h0_c[4];
	uint32_t Cb = h0_c[5];
	uint32_t Da = h0_c[6];
	uint32_t Db = h0_c[7];
	uint32_t Ea = h0_c[8];
	uint32_t Eb = h0_c[9];
	uint32_t Fa = h0_c[10];
	uint32_t Fb = h0_c[11];
	uint32_t Ga = h0_c[12];
	uint32_t Gb = h0_c[13];
	uint32_t Ha = h0_c[14];
	uint32_t Hb = h0_c[15];

	uint32_t Wi[128];



	I(words[29], words[28], words[19], words[18], words[3], words[2], words[1], words[0], Wi[1], Wi[0]);

	I(words[31], words[30], words[21], words[20], words[5], words[4], words[3], words[2], Wi[3], Wi[2]);

	I(Wi[1], Wi[0], words[23], words[22], words[7], words[6], words[5], words[4], Wi[5], Wi[4]);
	I(Wi[3], Wi[2], words[25], words[24], words[9], words[8], words[7], words[6], Wi[7], Wi[6]);
	I(Wi[5], Wi[4], words[27], words[26], words[11], words[10], words[9], words[8], Wi[9], Wi[8]);
	I(Wi[7], Wi[6], words[29], words[28], words[13], words[12], words[11], words[10], Wi[11], Wi[10]);
	I(Wi[9], Wi[8], words[31], words[30], words[15], words[14], words[13], words[12], Wi[13], Wi[12]);

	I(Wi[11], Wi[10], Wi[1], Wi[0], words[17], words[16], words[15], words[14], Wi[15], Wi[14]);
	I(Wi[13], Wi[12], Wi[3], Wi[2], words[19], words[18], words[17], words[16], Wi[17], Wi[16]);
	I(Wi[15], Wi[14], Wi[5], Wi[4], words[21], words[20], words[19], words[18], Wi[19], Wi[18]);
	I(Wi[17], Wi[16], Wi[7], Wi[6], words[23], words[22], words[21], words[20], Wi[21], Wi[20]);
	I(Wi[19], Wi[18], Wi[9], Wi[8], words[25], words[24], words[23], words[22], Wi[23], Wi[22]);
	I(Wi[21], Wi[20], Wi[11], Wi[10], words[27], words[26], words[25], words[24], Wi[25], Wi[24]);
	I(Wi[23], Wi[22], Wi[13], Wi[12], words[29], words[28], words[27], words[26], Wi[27], Wi[26]);
	I(Wi[25], Wi[24], Wi[15], Wi[14], words[31], words[30], words[29], words[28], Wi[29], Wi[28]);

	I(Wi[27], Wi[26], Wi[17], Wi[16], Wi[1], Wi[0], words[31], words[30], Wi[31], Wi[30]);

	I(Wi[29], Wi[28], Wi[19], Wi[18], Wi[3], Wi[2], Wi[1], Wi[0], Wi[33], Wi[32]);
	I(Wi[31], Wi[30], Wi[21], Wi[20], Wi[5], Wi[4], Wi[3], Wi[2], Wi[35], Wi[34]);
	I(Wi[33], Wi[32], Wi[23], Wi[22], Wi[7], Wi[6], Wi[5], Wi[4], Wi[37], Wi[36]);
	I(Wi[35], Wi[34], Wi[25], Wi[24], Wi[9], Wi[8], Wi[7], Wi[6], Wi[39], Wi[38]);
	I(Wi[37], Wi[36], Wi[27], Wi[26], Wi[11], Wi[10], Wi[9], Wi[8], Wi[41], Wi[40]);
	I(Wi[39], Wi[38], Wi[29], Wi[28], Wi[13], Wi[12], Wi[11], Wi[10], Wi[43], Wi[42]);
	I(Wi[41], Wi[40], Wi[31], Wi[30], Wi[15], Wi[14], Wi[13], Wi[12], Wi[45], Wi[44]);
	I(Wi[43], Wi[42], Wi[33], Wi[32], Wi[17], Wi[16], Wi[15], Wi[14], Wi[47], Wi[46]);
	I(Wi[45], Wi[44], Wi[35], Wi[34], Wi[19], Wi[18], Wi[17], Wi[16], Wi[49], Wi[48]);
	I(Wi[47], Wi[46], Wi[37], Wi[36], Wi[21], Wi[20], Wi[19], Wi[18], Wi[51], Wi[50]);
	I(Wi[49], Wi[48], Wi[39], Wi[38], Wi[23], Wi[22], Wi[21], Wi[20], Wi[53], Wi[52]);
	I(Wi[51], Wi[50], Wi[41], Wi[40], Wi[25], Wi[24], Wi[23], Wi[22], Wi[55], Wi[54]);
	I(Wi[53], Wi[52], Wi[43], Wi[42], Wi[27], Wi[26], Wi[25], Wi[24], Wi[57], Wi[56]);
	I(Wi[55], Wi[54], Wi[45], Wi[44], Wi[29], Wi[28], Wi[27], Wi[26], Wi[59], Wi[58]);
	I(Wi[57], Wi[56], Wi[47], Wi[46], Wi[31], Wi[30], Wi[29], Wi[28], Wi[61], Wi[60]);
	I(Wi[59], Wi[58], Wi[49], Wi[48], Wi[33], Wi[32], Wi[31], Wi[30], Wi[63], Wi[62]);
	I(Wi[61], Wi[60], Wi[51], Wi[50], Wi[35], Wi[34], Wi[33], Wi[32], Wi[65], Wi[64]);
	I(Wi[63], Wi[62], Wi[53], Wi[52], Wi[37], Wi[36], Wi[35], Wi[34], Wi[67], Wi[66]);
	I(Wi[65], Wi[64], Wi[55], Wi[54], Wi[39], Wi[38], Wi[37], Wi[36], Wi[69], Wi[68]);
	I(Wi[67], Wi[66], Wi[57], Wi[56], Wi[41], Wi[40], Wi[39], Wi[38], Wi[71], Wi[70]);
	I(Wi[69], Wi[68], Wi[59], Wi[58], Wi[43], Wi[42], Wi[41], Wi[40], Wi[73], Wi[72]);
	I(Wi[71], Wi[70], Wi[61], Wi[60], Wi[45], Wi[44], Wi[43], Wi[42], Wi[75], Wi[74]);
	I(Wi[73], Wi[72], Wi[63], Wi[62], Wi[47], Wi[46], Wi[45], Wi[44], Wi[77], Wi[76]);
	I(Wi[75], Wi[74], Wi[65], Wi[64], Wi[49], Wi[48], Wi[47], Wi[46], Wi[79], Wi[78]);
	I(Wi[77], Wi[76], Wi[67], Wi[66], Wi[51], Wi[50], Wi[49], Wi[48], Wi[81], Wi[80]);
	I(Wi[79], Wi[78], Wi[69], Wi[68], Wi[53], Wi[52], Wi[51], Wi[50], Wi[83], Wi[82]);
	I(Wi[81], Wi[80], Wi[71], Wi[70], Wi[55], Wi[54], Wi[53], Wi[52], Wi[85], Wi[84]);
	I(Wi[83], Wi[82], Wi[73], Wi[72], Wi[57], Wi[56], Wi[55], Wi[54], Wi[87], Wi[86]);
	I(Wi[85], Wi[84], Wi[75], Wi[74], Wi[59], Wi[58], Wi[57], Wi[56], Wi[89], Wi[88]);
	I(Wi[87], Wi[86], Wi[77], Wi[76], Wi[61], Wi[60], Wi[59], Wi[58], Wi[91], Wi[90]);
	I(Wi[89], Wi[88], Wi[79], Wi[78], Wi[63], Wi[62], Wi[61], Wi[60], Wi[93], Wi[92]);
	I(Wi[91], Wi[90], Wi[81], Wi[80], Wi[65], Wi[64], Wi[63], Wi[62], Wi[95], Wi[94]);
	I(Wi[93], Wi[92], Wi[83], Wi[82], Wi[67], Wi[66], Wi[65], Wi[64], Wi[97], Wi[96]);
	I(Wi[95], Wi[94], Wi[85], Wi[84], Wi[69], Wi[68], Wi[67], Wi[66], Wi[99], Wi[98]);
	I(Wi[97], Wi[96], Wi[87], Wi[86], Wi[71], Wi[70], Wi[69], Wi[68], Wi[101], Wi[100]);
	I(Wi[99], Wi[98], Wi[89], Wi[88], Wi[73], Wi[72], Wi[71], Wi[70], Wi[103], Wi[102]);
	I(Wi[101], Wi[100], Wi[91], Wi[90], Wi[75], Wi[74], Wi[73], Wi[72], Wi[105], Wi[104]);
	I(Wi[103], Wi[102], Wi[93], Wi[92], Wi[77], Wi[76], Wi[75], Wi[74], Wi[107], Wi[106]);
	I(Wi[105], Wi[104], Wi[95], Wi[94], Wi[79], Wi[78], Wi[77], Wi[76], Wi[109], Wi[108]);
	I(Wi[107], Wi[106], Wi[97], Wi[96], Wi[81], Wi[80], Wi[79], Wi[78], Wi[111], Wi[110]);
	I(Wi[109], Wi[108], Wi[99], Wi[98], Wi[83], Wi[82], Wi[81], Wi[80], Wi[113], Wi[112]);
	I(Wi[111], Wi[110], Wi[101], Wi[100], Wi[85], Wi[84], Wi[83], Wi[82], Wi[115], Wi[114]);
	I(Wi[113], Wi[112], Wi[103], Wi[102], Wi[87], Wi[86], Wi[85], Wi[84], Wi[117], Wi[116]);
	I(Wi[115], Wi[114], Wi[105], Wi[104], Wi[89], Wi[88], Wi[87], Wi[86], Wi[119], Wi[118]);
	I(Wi[117], Wi[116], Wi[107], Wi[106], Wi[91], Wi[90], Wi[89], Wi[88], Wi[121], Wi[120]);
	I(Wi[119], Wi[118], Wi[109], Wi[108], Wi[93], Wi[92], Wi[91], Wi[90], Wi[123], Wi[122]);
	I(Wi[121], Wi[120], Wi[111], Wi[110], Wi[95], Wi[94], Wi[93], Wi[92], Wi[125], Wi[124]);
	I(Wi[123], Wi[122], Wi[113], Wi[112], Wi[97], Wi[96], Wi[95], Wi[94], Wi[127], Wi[126]);
	//printf("%08x %08x %08x %08x %08x %08x %08x %08x\n", Wi[0], Wi[1], Wi[2], Wi[30], Wi[16], Wi[17], Wi[6], Wi[7]);
	//for (int i = 0; i < 128;i+=2) {
	//	printf("%08x %08x\n",Wi[i], Wi[i+1]);
	//}
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, words[1], words[0], k[0], k[1]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, words[3], words[2], k[2], k[3]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, words[5], words[4], k[4], k[5]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, words[7], words[6], k[6], k[7]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, words[9], words[8], k[8], k[9]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, words[11], words[10], k[10], k[11]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, words[13], words[12], k[12], k[13]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, words[15], words[14], k[14], k[15]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, words[17], words[16], k[16], k[17]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, words[19], words[18], k[18], k[19]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, words[21], words[20], k[20], k[21]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, words[23], words[22], k[22], k[23]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, words[25], words[24], k[24], k[25]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, words[27], words[26], k[26], k[27]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, words[29], words[28], k[28], k[29]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, words[31], words[30], k[30], k[31]);

	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[1], Wi[0], k[32], k[33]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[3], Wi[2], k[34], k[35]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[5], Wi[4], k[36], k[37]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[7], Wi[6], k[38], k[39]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[9], Wi[8], k[40], k[41]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[11], Wi[10], k[42], k[43]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[13], Wi[12], k[44], k[45]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[15], Wi[14], k[46], k[47]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[17], Wi[16], k[48], k[49]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[19], Wi[18], k[50], k[51]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[21], Wi[20], k[52], k[53]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[23], Wi[22], k[54], k[55]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[25], Wi[24], k[56], k[57]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[27], Wi[26], k[58], k[59]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[29], Wi[28], k[60], k[61]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[31], Wi[30], k[62], k[63]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[33], Wi[32], k[64], k[65]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[35], Wi[34], k[66], k[67]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[37], Wi[36], k[68], k[69]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[39], Wi[38], k[70], k[71]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[41], Wi[40], k[72], k[73]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[43], Wi[42], k[74], k[75]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[45], Wi[44], k[76], k[77]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[47], Wi[46], k[78], k[79]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[49], Wi[48], k[80], k[81]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[51], Wi[50], k[82], k[83]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[53], Wi[52], k[84], k[85]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[55], Wi[54], k[86], k[87]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[57], Wi[56], k[88], k[89]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[59], Wi[58], k[90], k[91]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[61], Wi[60], k[92], k[93]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[63], Wi[62], k[94], k[95]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[65], Wi[64], k[96], k[97]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[67], Wi[66], k[98], k[99]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[69], Wi[68], k[100], k[101]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[71], Wi[70], k[102], k[103]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[73], Wi[72], k[104], k[105]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[75], Wi[74], k[106], k[107]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[77], Wi[76], k[108], k[109]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[79], Wi[78], k[110], k[111]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[81], Wi[80], k[112], k[113]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[83], Wi[82], k[114], k[115]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[85], Wi[84], k[116], k[117]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[87], Wi[86], k[118], k[119]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[89], Wi[88], k[120], k[121]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[91], Wi[90], k[122], k[123]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[93], Wi[92], k[124], k[125]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[95], Wi[94], k[126], k[127]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[97], Wi[96], k[128], k[129]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[99], Wi[98], k[130], k[131]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[101], Wi[100], k[132], k[133]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[103], Wi[102], k[134], k[135]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[105], Wi[104], k[136], k[137]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[107], Wi[106], k[138], k[139]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[109], Wi[108], k[140], k[141]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[111], Wi[110], k[142], k[143]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[113], Wi[112], k[144], k[145]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[115], Wi[114], k[146], k[147]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[117], Wi[116], k[148], k[149]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[119], Wi[118], k[150], k[151]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[121], Wi[120], k[152], k[153]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[123], Wi[122], k[154], k[155]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[125], Wi[124], k[156], k[157]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[127], Wi[126], k[158], k[159]);


	

	SUM(Aa, Ab, h0_c[0], h0_c[1], words[1], words[0]);
	SUM(Ba, Bb, h0_c[2], h0_c[3], words[3], words[2]);
	SUM(Ca, Cb, h0_c[4], h0_c[5], words[5], words[4]);
	SUM(Da, Db, h0_c[6], h0_c[7], words[7], words[6]);
	SUM(Ea, Eb, h0_c[8], h0_c[9], words[9], words[8]);
	SUM(Fa, Fb, h0_c[10], h0_c[11], words[11], words[10]);
	SUM(Ga, Gb, h0_c[12], h0_c[13], words[13], words[12]);
	SUM(Ha, Hb, h0_c[14], h0_c[15], words[15], words[14]);


}


__device__ void sha512Faster(uint32_t* words) {

	uint32_t temp1A, temp1B, temp2A, temp2B, tsum1a, tsum1b, tsum2a, tsum2b, z1a, z2a, z3a, z1b, z2b, z3b;

#define P(aA, aB, bA, bB, cA, cB, dA, dB, eA, eB, fA, fB, gA, gB, hA, hB, xA, xB, KA, KB)					\
	{																											\
		SUM (hA, hB, S3A(eA, eB), S3B(eA, eB), z1a, z1b );														\
		SUM (F1(eA,fA,gA), F1(eB,fB,gB), KA, KB, z2a, z2b );													\
		SUM (z1a, z1b, z2a, z2b, z3a, z3b );																	\
		SUM (z3a, z3b, xA, xB, temp1A, temp1B);																	\
		SUM (S2A(aA, aB), S2B(aA, aB), F0(aA,bA,cA), F0(aB,bB,cB), temp2A, temp2B );							\
		SUM (dA, dB, temp1A, temp1B, dA, dB );																	\
		SUM (temp1A, temp1B, temp2A, temp2B, hA, hB);															\
	}

#define I(a0, a1, b0, b1, c0, c1, d0, d1, w0, w1)				\
	{																\
	SUM(S1A(a0, a1), S1B(a0, a1), b0, b1, tsum1a, tsum1b);			\
	SUM(S0A(c0, c1), S0B(c0, c1), d0, d1, tsum2a, tsum2b);			\
	SUM(tsum1a, tsum1b, tsum2a, tsum2b, w0, w1);					\
	}


	uint32_t Aa = h0_c[0];
	uint32_t Ab = h0_c[1];
	uint32_t Ba = h0_c[2];
	uint32_t Bb = h0_c[3];
	uint32_t Ca = h0_c[4];
	uint32_t Cb = h0_c[5];
	uint32_t Da = h0_c[6];
	uint32_t Db = h0_c[7];
	uint32_t Ea = h0_c[8];
	uint32_t Eb = h0_c[9];
	uint32_t Fa = h0_c[10];
	uint32_t Fb = h0_c[11];
	uint32_t Ga = h0_c[12];
	uint32_t Gb = h0_c[13];
	uint32_t Ha = h0_c[14];
	uint32_t Hb = h0_c[15];

	uint32_t Wi[128];


	//2147483648
	int zero = 0;
	I(zero, zero, zero, zero, words[3], words[2], words[1], words[0], Wi[1], Wi[0]);

	I(512, zero, zero, zero, words[5], words[4], words[3], words[2], Wi[3], Wi[2]);

	I(Wi[1], Wi[0], zero, zero, words[7], words[6], words[5], words[4], Wi[5], Wi[4]);
	I(Wi[3], Wi[2], zero, zero, words[9], words[8], words[7], words[6], Wi[7], Wi[6]);
	I(Wi[5], Wi[4], zero, zero, words[11], words[10], words[9], words[8], Wi[9], Wi[8]);
	I(Wi[7], Wi[6], zero, zero, words[13], words[12], words[11], words[10], Wi[11], Wi[10]);
	I(Wi[9], Wi[8],512, zero, words[15], words[14], words[13], words[12], Wi[13], Wi[12]);

	I(Wi[11], Wi[10], Wi[1], Wi[0], zero, 2147483648, words[15], words[14], Wi[15], Wi[14]);
	I(Wi[13], Wi[12], Wi[3], Wi[2], zero, zero, zero, 2147483648, Wi[17], Wi[16]);
	I(Wi[15], Wi[14], Wi[5], Wi[4], zero, zero, zero, zero, Wi[19], Wi[18]);
	I(Wi[17], Wi[16], Wi[7], Wi[6], zero, zero, zero, zero, Wi[21], Wi[20]);
	I(Wi[19], Wi[18], Wi[9], Wi[8], zero, zero, zero, zero, Wi[23], Wi[22]);
	I(Wi[21], Wi[20], Wi[11], Wi[10], zero, zero, zero, zero, Wi[25], Wi[24]);
	I(Wi[23], Wi[22], Wi[13], Wi[12], zero, zero, zero, zero, Wi[27], Wi[26]);
	I(Wi[25], Wi[24], Wi[15], Wi[14], 512, zero, zero, zero, Wi[29], Wi[28]);

	I(Wi[27], Wi[26], Wi[17], Wi[16], Wi[1], Wi[0], 512, zero, Wi[31], Wi[30]);

	I(Wi[29], Wi[28], Wi[19], Wi[18], Wi[3], Wi[2], Wi[1], Wi[0], Wi[33], Wi[32]);
	I(Wi[31], Wi[30], Wi[21], Wi[20], Wi[5], Wi[4], Wi[3], Wi[2], Wi[35], Wi[34]);
	I(Wi[33], Wi[32], Wi[23], Wi[22], Wi[7], Wi[6], Wi[5], Wi[4], Wi[37], Wi[36]);
	I(Wi[35], Wi[34], Wi[25], Wi[24], Wi[9], Wi[8], Wi[7], Wi[6], Wi[39], Wi[38]);
	I(Wi[37], Wi[36], Wi[27], Wi[26], Wi[11], Wi[10], Wi[9], Wi[8], Wi[41], Wi[40]);
	I(Wi[39], Wi[38], Wi[29], Wi[28], Wi[13], Wi[12], Wi[11], Wi[10], Wi[43], Wi[42]);
	I(Wi[41], Wi[40], Wi[31], Wi[30], Wi[15], Wi[14], Wi[13], Wi[12], Wi[45], Wi[44]);
	I(Wi[43], Wi[42], Wi[33], Wi[32], Wi[17], Wi[16], Wi[15], Wi[14], Wi[47], Wi[46]);
	I(Wi[45], Wi[44], Wi[35], Wi[34], Wi[19], Wi[18], Wi[17], Wi[16], Wi[49], Wi[48]);
	I(Wi[47], Wi[46], Wi[37], Wi[36], Wi[21], Wi[20], Wi[19], Wi[18], Wi[51], Wi[50]);
	I(Wi[49], Wi[48], Wi[39], Wi[38], Wi[23], Wi[22], Wi[21], Wi[20], Wi[53], Wi[52]);
	I(Wi[51], Wi[50], Wi[41], Wi[40], Wi[25], Wi[24], Wi[23], Wi[22], Wi[55], Wi[54]);
	I(Wi[53], Wi[52], Wi[43], Wi[42], Wi[27], Wi[26], Wi[25], Wi[24], Wi[57], Wi[56]);
	I(Wi[55], Wi[54], Wi[45], Wi[44], Wi[29], Wi[28], Wi[27], Wi[26], Wi[59], Wi[58]);
	I(Wi[57], Wi[56], Wi[47], Wi[46], Wi[31], Wi[30], Wi[29], Wi[28], Wi[61], Wi[60]);
	I(Wi[59], Wi[58], Wi[49], Wi[48], Wi[33], Wi[32], Wi[31], Wi[30], Wi[63], Wi[62]);
	I(Wi[61], Wi[60], Wi[51], Wi[50], Wi[35], Wi[34], Wi[33], Wi[32], Wi[65], Wi[64]);
	I(Wi[63], Wi[62], Wi[53], Wi[52], Wi[37], Wi[36], Wi[35], Wi[34], Wi[67], Wi[66]);
	I(Wi[65], Wi[64], Wi[55], Wi[54], Wi[39], Wi[38], Wi[37], Wi[36], Wi[69], Wi[68]);
	I(Wi[67], Wi[66], Wi[57], Wi[56], Wi[41], Wi[40], Wi[39], Wi[38], Wi[71], Wi[70]);
	I(Wi[69], Wi[68], Wi[59], Wi[58], Wi[43], Wi[42], Wi[41], Wi[40], Wi[73], Wi[72]);
	I(Wi[71], Wi[70], Wi[61], Wi[60], Wi[45], Wi[44], Wi[43], Wi[42], Wi[75], Wi[74]);
	I(Wi[73], Wi[72], Wi[63], Wi[62], Wi[47], Wi[46], Wi[45], Wi[44], Wi[77], Wi[76]);
	I(Wi[75], Wi[74], Wi[65], Wi[64], Wi[49], Wi[48], Wi[47], Wi[46], Wi[79], Wi[78]);
	I(Wi[77], Wi[76], Wi[67], Wi[66], Wi[51], Wi[50], Wi[49], Wi[48], Wi[81], Wi[80]);
	I(Wi[79], Wi[78], Wi[69], Wi[68], Wi[53], Wi[52], Wi[51], Wi[50], Wi[83], Wi[82]);
	I(Wi[81], Wi[80], Wi[71], Wi[70], Wi[55], Wi[54], Wi[53], Wi[52], Wi[85], Wi[84]);
	I(Wi[83], Wi[82], Wi[73], Wi[72], Wi[57], Wi[56], Wi[55], Wi[54], Wi[87], Wi[86]);
	I(Wi[85], Wi[84], Wi[75], Wi[74], Wi[59], Wi[58], Wi[57], Wi[56], Wi[89], Wi[88]);
	I(Wi[87], Wi[86], Wi[77], Wi[76], Wi[61], Wi[60], Wi[59], Wi[58], Wi[91], Wi[90]);
	I(Wi[89], Wi[88], Wi[79], Wi[78], Wi[63], Wi[62], Wi[61], Wi[60], Wi[93], Wi[92]);
	I(Wi[91], Wi[90], Wi[81], Wi[80], Wi[65], Wi[64], Wi[63], Wi[62], Wi[95], Wi[94]);
	I(Wi[93], Wi[92], Wi[83], Wi[82], Wi[67], Wi[66], Wi[65], Wi[64], Wi[97], Wi[96]);
	I(Wi[95], Wi[94], Wi[85], Wi[84], Wi[69], Wi[68], Wi[67], Wi[66], Wi[99], Wi[98]);
	I(Wi[97], Wi[96], Wi[87], Wi[86], Wi[71], Wi[70], Wi[69], Wi[68], Wi[101], Wi[100]);
	I(Wi[99], Wi[98], Wi[89], Wi[88], Wi[73], Wi[72], Wi[71], Wi[70], Wi[103], Wi[102]);
	I(Wi[101], Wi[100], Wi[91], Wi[90], Wi[75], Wi[74], Wi[73], Wi[72], Wi[105], Wi[104]);
	I(Wi[103], Wi[102], Wi[93], Wi[92], Wi[77], Wi[76], Wi[75], Wi[74], Wi[107], Wi[106]);
	I(Wi[105], Wi[104], Wi[95], Wi[94], Wi[79], Wi[78], Wi[77], Wi[76], Wi[109], Wi[108]);
	I(Wi[107], Wi[106], Wi[97], Wi[96], Wi[81], Wi[80], Wi[79], Wi[78], Wi[111], Wi[110]);
	I(Wi[109], Wi[108], Wi[99], Wi[98], Wi[83], Wi[82], Wi[81], Wi[80], Wi[113], Wi[112]);
	I(Wi[111], Wi[110], Wi[101], Wi[100], Wi[85], Wi[84], Wi[83], Wi[82], Wi[115], Wi[114]);
	I(Wi[113], Wi[112], Wi[103], Wi[102], Wi[87], Wi[86], Wi[85], Wi[84], Wi[117], Wi[116]);
	I(Wi[115], Wi[114], Wi[105], Wi[104], Wi[89], Wi[88], Wi[87], Wi[86], Wi[119], Wi[118]);
	I(Wi[117], Wi[116], Wi[107], Wi[106], Wi[91], Wi[90], Wi[89], Wi[88], Wi[121], Wi[120]);
	I(Wi[119], Wi[118], Wi[109], Wi[108], Wi[93], Wi[92], Wi[91], Wi[90], Wi[123], Wi[122]);
	I(Wi[121], Wi[120], Wi[111], Wi[110], Wi[95], Wi[94], Wi[93], Wi[92], Wi[125], Wi[124]);
	I(Wi[123], Wi[122], Wi[113], Wi[112], Wi[97], Wi[96], Wi[95], Wi[94], Wi[127], Wi[126]);
	//printf("%08x %08x %08x %08x %08x %08x %08x %08x\n", Wi[0], Wi[1], Wi[2], Wi[30], Wi[16], Wi[17], Wi[6], Wi[7]);
	//for (int i = 0; i < 128;i+=2) {
	//	printf("%08x %08x\n",Wi[i], Wi[i+1]);
	//}
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, words[1], words[0], k[0], k[1]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, words[3], words[2], k[2], k[3]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, words[5], words[4], k[4], k[5]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, words[7], words[6], k[6], k[7]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, words[9], words[8], k[8], k[9]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, words[11], words[10], k[10], k[11]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, words[13], words[12], k[12], k[13]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, words[15], words[14], k[14], k[15]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, zero, 2147483648, k[16], k[17]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, zero, zero, k[18], k[19]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, zero, zero, k[20], k[21]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, zero, zero, k[22], k[23]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, zero, zero, k[24], k[25]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, zero, zero, k[26], k[27]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, zero, zero, k[28], k[29]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, 512, zero, k[30], k[31]);

	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[1], Wi[0], k[32], k[33]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[3], Wi[2], k[34], k[35]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[5], Wi[4], k[36], k[37]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[7], Wi[6], k[38], k[39]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[9], Wi[8], k[40], k[41]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[11], Wi[10], k[42], k[43]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[13], Wi[12], k[44], k[45]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[15], Wi[14], k[46], k[47]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[17], Wi[16], k[48], k[49]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[19], Wi[18], k[50], k[51]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[21], Wi[20], k[52], k[53]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[23], Wi[22], k[54], k[55]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[25], Wi[24], k[56], k[57]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[27], Wi[26], k[58], k[59]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[29], Wi[28], k[60], k[61]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[31], Wi[30], k[62], k[63]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[33], Wi[32], k[64], k[65]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[35], Wi[34], k[66], k[67]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[37], Wi[36], k[68], k[69]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[39], Wi[38], k[70], k[71]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[41], Wi[40], k[72], k[73]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[43], Wi[42], k[74], k[75]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[45], Wi[44], k[76], k[77]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[47], Wi[46], k[78], k[79]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[49], Wi[48], k[80], k[81]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[51], Wi[50], k[82], k[83]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[53], Wi[52], k[84], k[85]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[55], Wi[54], k[86], k[87]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[57], Wi[56], k[88], k[89]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[59], Wi[58], k[90], k[91]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[61], Wi[60], k[92], k[93]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[63], Wi[62], k[94], k[95]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[65], Wi[64], k[96], k[97]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[67], Wi[66], k[98], k[99]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[69], Wi[68], k[100], k[101]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[71], Wi[70], k[102], k[103]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[73], Wi[72], k[104], k[105]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[75], Wi[74], k[106], k[107]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[77], Wi[76], k[108], k[109]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[79], Wi[78], k[110], k[111]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[81], Wi[80], k[112], k[113]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[83], Wi[82], k[114], k[115]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[85], Wi[84], k[116], k[117]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[87], Wi[86], k[118], k[119]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[89], Wi[88], k[120], k[121]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[91], Wi[90], k[122], k[123]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[93], Wi[92], k[124], k[125]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[95], Wi[94], k[126], k[127]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[97], Wi[96], k[128], k[129]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[99], Wi[98], k[130], k[131]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[101], Wi[100], k[132], k[133]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[103], Wi[102], k[134], k[135]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[105], Wi[104], k[136], k[137]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[107], Wi[106], k[138], k[139]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[109], Wi[108], k[140], k[141]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[111], Wi[110], k[142], k[143]);
	P(Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Wi[113], Wi[112], k[144], k[145]);
	P(Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Wi[115], Wi[114], k[146], k[147]);
	P(Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Wi[117], Wi[116], k[148], k[149]);
	P(Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Wi[119], Wi[118], k[150], k[151]);
	P(Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Da, Db, Wi[121], Wi[120], k[152], k[153]);
	P(Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Ca, Cb, Wi[123], Wi[122], k[154], k[155]);
	P(Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Ba, Bb, Wi[125], Wi[124], k[156], k[157]);
	P(Ba, Bb, Ca, Cb, Da, Db, Ea, Eb, Fa, Fb, Ga, Gb, Ha, Hb, Aa, Ab, Wi[127], Wi[126], k[158], k[159]);




	SUM(Aa, Ab, h0_c[0], h0_c[1], words[1], words[0]);
	SUM(Ba, Bb, h0_c[2], h0_c[3], words[3], words[2]);
	SUM(Ca, Cb, h0_c[4], h0_c[5], words[5], words[4]);
	SUM(Da, Db, h0_c[6], h0_c[7], words[7], words[6]);
	SUM(Ea, Eb, h0_c[8], h0_c[9], words[9], words[8]);
	SUM(Fa, Fb, h0_c[10], h0_c[11], words[11], words[10]);
	SUM(Ga, Gb, h0_c[12], h0_c[13], words[13], words[12]);
	SUM(Ha, Hb, h0_c[14], h0_c[15], words[15], words[14]);


}

#endif