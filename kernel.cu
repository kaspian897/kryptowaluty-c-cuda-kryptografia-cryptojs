#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cuda.h>
#include <inttypes.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "md5.cuh"
#include "sha512.cuh"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

static const int B64index[256] = { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 62, 63, 62, 62, 63, 52, 53, 54, 55,
56, 57, 58, 59, 60, 61,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,
7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  0,
0,  0,  0, 63,  0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };

std::string b64decode(const void* data, const size_t len)
{
    unsigned char* p = (unsigned char*)data;

    int pad = len > 0 && (len % 4 || p[len - 1] == '=');
    const size_t L = ((len + 3) / 4 - pad) * 4;
    std::string str(L / 4 * 3 + pad, '\0');

    for (size_t i = 0, j = 0; i < L; i += 4)
    {
        int n = B64index[p[i]] << 18 | B64index[p[i + 1]] << 12 | B64index[p[i + 2]] << 6 | B64index[p[i + 3]];
        str[j++] = n >> 16;
        str[j++] = n >> 8 & 0xFF;
        str[j++] = n & 0xFF;
    }
    if (pad)
    {
        int n = B64index[p[L]] << 18 | B64index[p[L + 1]] << 12;
        str[str.size() - 1] = n >> 16;

        if (len > L + 2 && p[L + 2] != '=')
        {
            n |= B64index[p[L + 2]] << 6;
            str.push_back(n >> 8 & 0xFF);
        }
    }
    return str;
}
cudaError_t addWithCuda(int* c, const int* a, const int* b, unsigned int size);

__device__ static const uint32_t r_con[32] = {
    0x00000000, 0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000,
    0x80000000, 0x1b000000, 0x36000000, 0x6c000000, 0xd8000000, 0xab000000, 0x4d000000, 0x9a000000,
    0x2f000000, 0x5e000000, 0xbc000000, 0x63000000, 0xc6000000, 0x97000000, 0x35000000, 0x6a000000,
    0xd4000000, 0xb3000000, 0x7d000000, 0xfa000000, 0xef000000, 0xc5000000, 0x91000000, 0x39000000,
};

__device__ static const uint8_t s_box[256] = {
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
};

__device__ static const uint8_t inv_s_box[256] = {
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d,
};

__device__ static const uint32_t mix_col[256] = {
    0x00000000, 0x02010103, 0x04020206, 0x06030305, 0x0804040c, 0x0a05050f, 0x0c06060a, 0x0e070709,
    0x10080818, 0x1209091b, 0x140a0a1e, 0x160b0b1d, 0x180c0c14, 0x1a0d0d17, 0x1c0e0e12, 0x1e0f0f11,
    0x20101030, 0x22111133, 0x24121236, 0x26131335, 0x2814143c, 0x2a15153f, 0x2c16163a, 0x2e171739,
    0x30181828, 0x3219192b, 0x341a1a2e, 0x361b1b2d, 0x381c1c24, 0x3a1d1d27, 0x3c1e1e22, 0x3e1f1f21,
    0x40202060, 0x42212163, 0x44222266, 0x46232365, 0x4824246c, 0x4a25256f, 0x4c26266a, 0x4e272769,
    0x50282878, 0x5229297b, 0x542a2a7e, 0x562b2b7d, 0x582c2c74, 0x5a2d2d77, 0x5c2e2e72, 0x5e2f2f71,
    0x60303050, 0x62313153, 0x64323256, 0x66333355, 0x6834345c, 0x6a35355f, 0x6c36365a, 0x6e373759,
    0x70383848, 0x7239394b, 0x743a3a4e, 0x763b3b4d, 0x783c3c44, 0x7a3d3d47, 0x7c3e3e42, 0x7e3f3f41,
    0x804040c0, 0x824141c3, 0x844242c6, 0x864343c5, 0x884444cc, 0x8a4545cf, 0x8c4646ca, 0x8e4747c9,
    0x904848d8, 0x924949db, 0x944a4ade, 0x964b4bdd, 0x984c4cd4, 0x9a4d4dd7, 0x9c4e4ed2, 0x9e4f4fd1,
    0xa05050f0, 0xa25151f3, 0xa45252f6, 0xa65353f5, 0xa85454fc, 0xaa5555ff, 0xac5656fa, 0xae5757f9,
    0xb05858e8, 0xb25959eb, 0xb45a5aee, 0xb65b5bed, 0xb85c5ce4, 0xba5d5de7, 0xbc5e5ee2, 0xbe5f5fe1,
    0xc06060a0, 0xc26161a3, 0xc46262a6, 0xc66363a5, 0xc86464ac, 0xca6565af, 0xcc6666aa, 0xce6767a9,
    0xd06868b8, 0xd26969bb, 0xd46a6abe, 0xd66b6bbd, 0xd86c6cb4, 0xda6d6db7, 0xdc6e6eb2, 0xde6f6fb1,
    0xe0707090, 0xe2717193, 0xe4727296, 0xe6737395, 0xe874749c, 0xea75759f, 0xec76769a, 0xee777799,
    0xf0787888, 0xf279798b, 0xf47a7a8e, 0xf67b7b8d, 0xf87c7c84, 0xfa7d7d87, 0xfc7e7e82, 0xfe7f7f81,
    0x1b80809b, 0x19818198, 0x1f82829d, 0x1d83839e, 0x13848497, 0x11858594, 0x17868691, 0x15878792,
    0x0b888883, 0x09898980, 0x0f8a8a85, 0x0d8b8b86, 0x038c8c8f, 0x018d8d8c, 0x078e8e89, 0x058f8f8a,
    0x3b9090ab, 0x399191a8, 0x3f9292ad, 0x3d9393ae, 0x339494a7, 0x319595a4, 0x379696a1, 0x359797a2,
    0x2b9898b3, 0x299999b0, 0x2f9a9ab5, 0x2d9b9bb6, 0x239c9cbf, 0x219d9dbc, 0x279e9eb9, 0x259f9fba,
    0x5ba0a0fb, 0x59a1a1f8, 0x5fa2a2fd, 0x5da3a3fe, 0x53a4a4f7, 0x51a5a5f4, 0x57a6a6f1, 0x55a7a7f2,
    0x4ba8a8e3, 0x49a9a9e0, 0x4faaaae5, 0x4dababe6, 0x43acacef, 0x41adadec, 0x47aeaee9, 0x45afafea,
    0x7bb0b0cb, 0x79b1b1c8, 0x7fb2b2cd, 0x7db3b3ce, 0x73b4b4c7, 0x71b5b5c4, 0x77b6b6c1, 0x75b7b7c2,
    0x6bb8b8d3, 0x69b9b9d0, 0x6fbabad5, 0x6dbbbbd6, 0x63bcbcdf, 0x61bdbddc, 0x67bebed9, 0x65bfbfda,
    0x9bc0c05b, 0x99c1c158, 0x9fc2c25d, 0x9dc3c35e, 0x93c4c457, 0x91c5c554, 0x97c6c651, 0x95c7c752,
    0x8bc8c843, 0x89c9c940, 0x8fcaca45, 0x8dcbcb46, 0x83cccc4f, 0x81cdcd4c, 0x87cece49, 0x85cfcf4a,
    0xbbd0d06b, 0xb9d1d168, 0xbfd2d26d, 0xbdd3d36e, 0xb3d4d467, 0xb1d5d564, 0xb7d6d661, 0xb5d7d762,
    0xabd8d873, 0xa9d9d970, 0xafdada75, 0xaddbdb76, 0xa3dcdc7f, 0xa1dddd7c, 0xa7dede79, 0xa5dfdf7a,
    0xdbe0e03b, 0xd9e1e138, 0xdfe2e23d, 0xdde3e33e, 0xd3e4e437, 0xd1e5e534, 0xd7e6e631, 0xd5e7e732,
    0xcbe8e823, 0xc9e9e920, 0xcfeaea25, 0xcdebeb26, 0xc3ecec2f, 0xc1eded2c, 0xc7eeee29, 0xc5efef2a,
    0xfbf0f00b, 0xf9f1f108, 0xfff2f20d, 0xfdf3f30e, 0xf3f4f407, 0xf1f5f504, 0xf7f6f601, 0xf5f7f702,
    0xebf8f813, 0xe9f9f910, 0xeffafa15, 0xedfbfb16, 0xe3fcfc1f, 0xe1fdfd1c, 0xe7fefe19, 0xe5ffff1a,
};

__device__ const uint32_t inv_mix_col[256] = {
    0x00000000, 0x0e090d0b, 0x1c121a16, 0x121b171d, 0x3824342c, 0x362d3927, 0x24362e3a, 0x2a3f2331,
    0x70486858, 0x7e416553, 0x6c5a724e, 0x62537f45, 0x486c5c74, 0x4665517f, 0x547e4662, 0x5a774b69,
    0xe090d0b0, 0xee99ddbb, 0xfc82caa6, 0xf28bc7ad, 0xd8b4e49c, 0xd6bde997, 0xc4a6fe8a, 0xcaaff381,
    0x90d8b8e8, 0x9ed1b5e3, 0x8ccaa2fe, 0x82c3aff5, 0xa8fc8cc4, 0xa6f581cf, 0xb4ee96d2, 0xbae79bd9,
    0xdb3bbb7b, 0xd532b670, 0xc729a16d, 0xc920ac66, 0xe31f8f57, 0xed16825c, 0xff0d9541, 0xf104984a,
    0xab73d323, 0xa57ade28, 0xb761c935, 0xb968c43e, 0x9357e70f, 0x9d5eea04, 0x8f45fd19, 0x814cf012,
    0x3bab6bcb, 0x35a266c0, 0x27b971dd, 0x29b07cd6, 0x038f5fe7, 0x0d8652ec, 0x1f9d45f1, 0x119448fa,
    0x4be30393, 0x45ea0e98, 0x57f11985, 0x59f8148e, 0x73c737bf, 0x7dce3ab4, 0x6fd52da9, 0x61dc20a2,
    0xad766df6, 0xa37f60fd, 0xb16477e0, 0xbf6d7aeb, 0x955259da, 0x9b5b54d1, 0x894043cc, 0x87494ec7,
    0xdd3e05ae, 0xd33708a5, 0xc12c1fb8, 0xcf2512b3, 0xe51a3182, 0xeb133c89, 0xf9082b94, 0xf701269f,
    0x4de6bd46, 0x43efb04d, 0x51f4a750, 0x5ffdaa5b, 0x75c2896a, 0x7bcb8461, 0x69d0937c, 0x67d99e77,
    0x3daed51e, 0x33a7d815, 0x21bccf08, 0x2fb5c203, 0x058ae132, 0x0b83ec39, 0x1998fb24, 0x1791f62f,
    0x764dd68d, 0x7844db86, 0x6a5fcc9b, 0x6456c190, 0x4e69e2a1, 0x4060efaa, 0x527bf8b7, 0x5c72f5bc,
    0x0605bed5, 0x080cb3de, 0x1a17a4c3, 0x141ea9c8, 0x3e218af9, 0x302887f2, 0x223390ef, 0x2c3a9de4,
    0x96dd063d, 0x98d40b36, 0x8acf1c2b, 0x84c61120, 0xaef93211, 0xa0f03f1a, 0xb2eb2807, 0xbce2250c,
    0xe6956e65, 0xe89c636e, 0xfa877473, 0xf48e7978, 0xdeb15a49, 0xd0b85742, 0xc2a3405f, 0xccaa4d54,
    0x41ecdaf7, 0x4fe5d7fc, 0x5dfec0e1, 0x53f7cdea, 0x79c8eedb, 0x77c1e3d0, 0x65daf4cd, 0x6bd3f9c6,
    0x31a4b2af, 0x3fadbfa4, 0x2db6a8b9, 0x23bfa5b2, 0x09808683, 0x07898b88, 0x15929c95, 0x1b9b919e,
    0xa17c0a47, 0xaf75074c, 0xbd6e1051, 0xb3671d5a, 0x99583e6b, 0x97513360, 0x854a247d, 0x8b432976,
    0xd134621f, 0xdf3d6f14, 0xcd267809, 0xc32f7502, 0xe9105633, 0xe7195b38, 0xf5024c25, 0xfb0b412e,
    0x9ad7618c, 0x94de6c87, 0x86c57b9a, 0x88cc7691, 0xa2f355a0, 0xacfa58ab, 0xbee14fb6, 0xb0e842bd,
    0xea9f09d4, 0xe49604df, 0xf68d13c2, 0xf8841ec9, 0xd2bb3df8, 0xdcb230f3, 0xcea927ee, 0xc0a02ae5,
    0x7a47b13c, 0x744ebc37, 0x6655ab2a, 0x685ca621, 0x42638510, 0x4c6a881b, 0x5e719f06, 0x5078920d,
    0x0a0fd964, 0x0406d46f, 0x161dc372, 0x1814ce79, 0x322bed48, 0x3c22e043, 0x2e39f75e, 0x2030fa55,
    0xec9ab701, 0xe293ba0a, 0xf088ad17, 0xfe81a01c, 0xd4be832d, 0xdab78e26, 0xc8ac993b, 0xc6a59430,
    0x9cd2df59, 0x92dbd252, 0x80c0c54f, 0x8ec9c844, 0xa4f6eb75, 0xaaffe67e, 0xb8e4f163, 0xb6edfc68,
    0x0c0a67b1, 0x02036aba, 0x10187da7, 0x1e1170ac, 0x342e539d, 0x3a275e96, 0x283c498b, 0x26354480,
    0x7c420fe9, 0x724b02e2, 0x605015ff, 0x6e5918f4, 0x44663bc5, 0x4a6f36ce, 0x587421d3, 0x567d2cd8,
    0x37a10c7a, 0x39a80171, 0x2bb3166c, 0x25ba1b67, 0x0f853856, 0x018c355d, 0x13972240, 0x1d9e2f4b,
    0x47e96422, 0x49e06929, 0x5bfb7e34, 0x55f2733f, 0x7fcd500e, 0x71c45d05, 0x63df4a18, 0x6dd64713,
    0xd731dcca, 0xd938d1c1, 0xcb23c6dc, 0xc52acbd7, 0xef15e8e6, 0xe11ce5ed, 0xf307f2f0, 0xfd0efffb,
    0xa779b492, 0xa970b999, 0xbb6bae84, 0xb562a38f, 0x9f5d80be, 0x91548db5, 0x834f9aa8, 0x8d4697a3,
};

// for key expansion
__device__ inline uint32_t rot_word(uint32_t v)
{
    return (v << 8) | (v >> 24);
}

// for mix columns
__device__ inline uint32_t rot8(uint32_t v)
{
    return (v >> 8) | (v << 24);
}

__device__ inline uint32_t rot16(uint32_t v)
{
    return (v >> 16) | (v << 16);
}

__device__ inline uint32_t rot24(uint32_t v)
{
    return (v >> 24) | (v << 8);
}
/////////////////////////////////////////////
static inline uint32_t l_pack(const uint8_t* b)
{
    return *(const uint32_t*)b;
}

__device__ inline void l_unpack(uint32_t v, uint8_t* b)
{
    *(uint32_t*)b = v;
}
/////////////////////////////////////////////


__device__ inline uint32_t pack(const uint8_t* b)
{
    return (((uint32_t)b[0]) << 24) |
        (((uint32_t)b[1]) << 16) |
        (((uint32_t)b[2]) << 8) |
        (((uint32_t)b[3]));
}

__device__ inline void unpack(uint32_t v, uint8_t* b)
{
    b[0] = (uint8_t)(v >> 24);
    b[1] = (uint8_t)(v >> 16);
    b[2] = (uint8_t)(v >> 8);
    b[3] = (uint8_t)v;
}
////////////////////////////////////////////
__device__ static inline void add_round_key(uint32_t s[4], const uint32_t* w)
{
    s[0] ^= w[0];
    s[1] ^= w[1];
    s[2] ^= w[2];
    s[3] ^= w[3];
}

__device__ inline uint32_t sub_word(uint32_t v)
{
    return (s_box[v >> 24] << 24) | (s_box[(v >> 16) & 0xff] << 16) | (s_box[(v >> 8) & 0xff] << 8) | (s_box[v & 0xff]);
}
////////////////////////////////////////////
__device__ void sub_bytes_shift_rows(uint8_t s[16])
{
    uint8_t	t;

    s[0] = s_box[s[0]];
    s[4] = s_box[s[4]];
    s[8] = s_box[s[8]];
    s[12] = s_box[s[12]];

    t = s_box[s[1]];
    s[1] = s_box[s[5]];
    s[5] = s_box[s[9]];
    s[9] = s_box[s[13]];
    s[13] = t;

    t = s_box[s[2]];
    s[2] = s_box[s[10]];
    s[10] = t;
    t = s_box[s[6]];
    s[6] = s_box[s[14]];
    s[14] = t;

    t = s_box[s[15]];
    s[15] = s_box[s[11]];
    s[11] = s_box[s[7]];
    s[7] = s_box[s[3]];
    s[3] = t;
}
//////////////////////////////////////////
__device__ void inv_shift_rows_inv_sub_bytes(uint8_t s[16])
{
    uint8_t	t;
    s[15] = inv_s_box[s[15]];
    s[11] = inv_s_box[s[11]];
    s[7] = inv_s_box[s[7]];
    s[3] = inv_s_box[s[3]];

    t = inv_s_box[s[2]];
    s[2] = inv_s_box[s[14]];
    s[14] = inv_s_box[s[10]];
    s[10] = inv_s_box[s[6]];
    s[6] = t;

    t = inv_s_box[s[1]];
    s[1] = inv_s_box[s[9]];
    s[9] = t;
    t = inv_s_box[s[13]];
    s[13] = inv_s_box[s[5]];
    s[5] = t;

    t = inv_s_box[s[0]];
    s[0] = inv_s_box[s[4]];
    s[4] = inv_s_box[s[8]];
    s[8] = inv_s_box[s[12]];
    s[12] = t;

    /*
    s[0] = inv_s_box[s[0]];
    s[4] = inv_s_box[s[4]];
    s[8] = inv_s_box[s[8]];
    s[12] = inv_s_box[s[12]];
    /*
    t = inv_s_box[s[13]];
    s[13] = inv_s_box[s[9]];
    s[9] = inv_s_box[s[5]];
    s[5] = inv_s_box[s[1]];
    s[1] = t;

    t = inv_s_box[s[14]];
    s[14] = inv_s_box[s[6]];
    s[6] = t;
    t = inv_s_box[s[10]];
    s[10] = inv_s_box[s[2]];
    s[2] = t;

    t = inv_s_box[s[3]];
    s[3] = inv_s_box[s[7]];
    s[7] = inv_s_box[s[11]];
    s[11] = inv_s_box[s[15]];
    s[15] = t;
    
    ////////////////
    
    t = inv_s_box[s[1]];
    s[1] = inv_s_box[s[5]];
    s[5] = inv_s_box[s[9]];
    s[9] = inv_s_box[s[13]];
    s[13] = t;

    t = inv_s_box[s[2]];
    s[2] = inv_s_box[s[10]];
    s[10] = t;
    t = inv_s_box[s[6]];
    s[6] = inv_s_box[s[14]];
    s[14] = t;
    */
   // t = inv_s_box[s[15]];
    //s[15] = inv_s_box[s[11]];
    //s[11] = inv_s_box[s[7]];
    //s[7] = inv_s_box[s[3]];
    //s[3] = t;
    //s[15] = inv_s_box[s[15]];
    //s[11] = inv_s_box[s[11]];
    //s[7] = inv_s_box[s[7]];
    //s[3] = inv_s_box[s[3]];
    
}
/////////////////////////////////////////
__device__ void mix_columns_add_round_key(uint8_t s[16], const uint32_t* w)
{
    uint32_t	r;
    int		i;

    for (i = 0; i < 4; i++, s += 4) {
        r = w[i] ^ (mix_col[s[0]]) ^ rot8(mix_col[s[1]]) ^ rot16(mix_col[s[2]]) ^ rot24(mix_col[s[3]]);
        l_unpack(r, s);
    }
}

__device__ void inv_mix_columns_add_round_key(uint8_t s[16], const uint32_t* dw)
{
    uint32_t	r1,r2,r3,r4;
    int		i;

    /*for (i = 0; i < 4; i++, s += 4) {
        r =  dw[i]^(inv_mix_col[s[3]]) ^ rot8(inv_mix_col[s[2]]) ^ rot16(inv_mix_col[s[1]]) ^ rot24(inv_mix_col[s[0]]);
        printf("inv: %x %x %x %x %x %x\n",r, dw[i],(inv_mix_col[s[3]]), rot8(inv_mix_col[s[2]]), rot16(inv_mix_col[s[1]]), rot24(inv_mix_col[s[0]]));
        l_unpack(r, s);
    }*/

    r1 = dw[0] ^ (inv_mix_col[inv_s_box[s[3]]]) ^ rot8(inv_mix_col[inv_s_box[s[14]]]) ^ rot16(inv_mix_col[inv_s_box[s[9]]]) ^ rot24(inv_mix_col[inv_s_box[s[4]]]);
    //printf("inv: %x %x %x %x %x %x\n",r1 , dw[0], (inv_mix_col[inv_s_box[s[3]]]), rot8(inv_mix_col[inv_s_box[s[14]]]), rot16(inv_mix_col[inv_s_box[s[9]]]), rot24(inv_mix_col[inv_s_box[s[4]]]));


    r2 = dw[3] ^ (inv_mix_col[inv_s_box[s[15]]]) ^ rot8(inv_mix_col[inv_s_box[s[10]]]) ^ rot16(inv_mix_col[inv_s_box[s[5]]]) ^ rot24(inv_mix_col[inv_s_box[s[0]]]);
    //printf("inv: %x %x %x %x %x %x\n", r2, dw[3], (inv_mix_col[inv_s_box[s[15]]]), rot8(inv_mix_col[inv_s_box[s[10]]]), rot16(inv_mix_col[inv_s_box[s[5]]]), rot24(inv_mix_col[inv_s_box[s[0]]]));

    

    r3 = dw[2] ^ (inv_mix_col[inv_s_box[s[11]]]) ^ rot8(inv_mix_col[inv_s_box[s[6]]]) ^ rot16(inv_mix_col[inv_s_box[s[1]]]) ^ rot24(inv_mix_col[inv_s_box[s[12]]]);
    //printf("inv: %x %x %x %x %x %x\n", r3, dw[2], (inv_mix_col[inv_s_box[s[11]]]), rot8(inv_mix_col[inv_s_box[s[6]]]), rot16(inv_mix_col[inv_s_box[s[1]]]), rot24(inv_mix_col[inv_s_box[s[12]]]));
    

    r4 = dw[1] ^ (inv_mix_col[inv_s_box[s[7]]]) ^ rot8(inv_mix_col[inv_s_box[s[2]]]) ^ rot16(inv_mix_col[inv_s_box[s[13]]]) ^ rot24(inv_mix_col[inv_s_box[s[8]]]);
    //printf("inv: %x %x %x %x %x %x\n", r4, dw[1], (inv_mix_col[inv_s_box[s[7]]]), rot8(inv_mix_col[inv_s_box[s[2]]]), rot16(inv_mix_col[inv_s_box[s[13]]]), rot24(inv_mix_col[inv_s_box[s[8]]]));
    
    l_unpack(r1, s);
    l_unpack(r2, s + 12);
    l_unpack(r3, s + 8);
    l_unpack(r4, s+4);
    /*r = dw[1] ^ (inv_mix_col[inv_s_box[s[2]]]) ^ rot8(inv_mix_col[inv_s_box[s[1]]]) ^ rot16(inv_mix_col[inv_s_box[s[0]]]) ^ rot24(inv_mix_col[inv_s_box[s[2]]]);
    l_unpack(r, s);
    s += 4;
    printf("inv: %x %x %x %x %x %x\n", r, dw[1], (inv_mix_col[inv_s_box[s[2]]]), rot8(inv_mix_col[s[2]]), rot16(inv_mix_col[s[1]]), rot24(inv_mix_col[s[0]]));

    r = dw[2] ^ (inv_mix_col[inv_s_box[s[1]]]) ^ rot8(inv_mix_col[inv_s_box[s[0]]]) ^ rot16(inv_mix_col[inv_s_box[s[3]]]) ^ rot24(inv_mix_col[inv_s_box[s[2]]]);
    l_unpack(r, s);
    s += 4;

    r = dw[3] ^ (inv_mix_col[inv_s_box[s[0]]]) ^ rot8(inv_mix_col[inv_s_box[s[3]]]) ^ rot16(inv_mix_col[inv_s_box[s[2]]]) ^ rot24(inv_mix_col[inv_s_box[s[1]]]);
    l_unpack(r, s);
    s += 4;*/
    //printf("\n");
}

__device__ void inv_mix_columns(uint32_t dw[4])
{   
    //printf("%08x %08x %08x %08x\n",dw[0], dw[1], dw[2], dw[3]);
    uint8_t	s[4];
    int		i;

    for (i = 0; i < 4; i++) {
        //printf("%x\n", dw[i]);
        unpack(dw[i], s);
        //printf("%x %x %x %x\n", s[0], s[1], s[2], s[3]);
        dw[i] = (inv_mix_col[s[0]]) ^ (rot8(inv_mix_col[s[1]]) )^ (rot16(inv_mix_col[s[2]]) ) ^ (rot24(inv_mix_col[s[3]]) );
        //printf("%08x %02x %02x %02x\n", dw[i], rot8(inv_mix_col[s[1]]), rot16(inv_mix_col[s[2]]), rot24(inv_mix_col[s[3]]));
    }
}
/////////////////////////////////////////
typedef struct {
    int		num_key;		// key length (4, 6, 8 word)
    int		num_round;		// number of round(10, 12, 14)
    int		num_block;		// block size (4 word, fixed)
    uint32_t	enc_key[256];		// encrypt key schedule
    uint32_t	dec_key[256];		// decrypt key schedule
} AesParam;

__device__ void expand_key(uint32_t* dec_key, uint32_t* enc_key, int key_length,const int num_round, uint32_t * key)
{
    const int num_block=4;
    uint32_t t;

    // expand key for encrypt
    for (int i = 0; i < key_length; i++) {
        //enc_key[i] = pack((const uint8_t*)&key[i * 4]);
        enc_key[i] = key[i];
    }
    for (int i = key_length; i < num_block * (num_round + 1); i++) {
        t = enc_key[i - 1];
        if ((i % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[i / key_length];
        }
        else if ((key_length > 6) && ((i % key_length) == 4)) {
            t = sub_word(t);
        }
        enc_key[i] = enc_key[i - key_length] ^ t;
    }

    // expand key for decrypt
    for (int i = 0; i < num_block * (num_round + 1); i++) {
        dec_key[i] = enc_key[i];
    }
    for (int i = 1; i < num_round; i++) {
        inv_mix_columns(&dec_key[i * num_block]);
    }
}
__device__ void expand_key_v2(uint32_t* dec_key, int key_length, const int num_round, uint32_t* key)
{
    uint32_t t;


    for (int i = 0; i < key_length; i++) {
        //enc_key[i] = pack((const uint8_t*)&key[i * 4]);
        dec_key[i] = key[i];
    }
    for (int i = key_length; i < 4 * (num_round + 1); i++) {
        t = dec_key[i - 1];
        if ((i % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[i / key_length];
        }
        else if ((key_length > 6) && ((i % key_length) == 4)) {
            t = sub_word(t);
        }
        dec_key[i] = dec_key[i - key_length] ^ t;
    }

    for (int i = 1; i < num_round; i++) {
        inv_mix_columns(&dec_key[i * 4]);
    }
}
__device__ void expand_key_v3(int key_length, const int num_round, uint32_t* key,uint32_t *last, uint32_t* indeks)
{
    uint32_t t;
    uint32_t a= (*indeks)/key_length;

    uint32_t i;
    if ((*indeks) < key_length) {
        last[0] = key[(*indeks)];
        last[1] = key[(*indeks)+1];
        last[2] = key[(*indeks)+2];
        last[3] = key[(*indeks)+3];
        //inv_mix_columns(&last[0]);
        (*indeks) += 4;
    }
    else{
        //printf("%i %i %i\n", (*indeks), a, (*indeks) - a * key_length);
        //t = last[3];
        i = ((*indeks) - key_length) % 32;
        t = key[((*indeks) - 1) % 32];
        if (((*indeks) % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[(*indeks) / key_length];
        }
        else if ( ((*indeks) % key_length) == 4) {
            t = sub_word(t);
        }
        last[0] = key[i]^ t;
        key[i] = last[0];

        
        ////////////////////////////////////////////
        (*indeks) += 1;

        t = last[0];
        //t = key[((*indeks) - 1) % 32];
        if (((*indeks) % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[(*indeks) / key_length];
        }
        else if (((*indeks) % key_length) == 4) {
            t = sub_word(t);
        }
        last[1] = key[i+1] ^ t;
        key[i+1] = last[1];
        ///////////////////////////////////////////////
        (*indeks) += 1;

        t = last[1];
        if (((*indeks) % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[(*indeks) / key_length];
        }
        else if (((*indeks) % key_length) == 4) {
            t = sub_word(t);
        }
        last[2] = key[i+2] ^ t;
        key[i+2] = last[2];
        ///////////////////////////////////////////////
        (*indeks) += 1;

        t = last[2];
        if (((*indeks) % key_length) == 0) {
            t = sub_word(rot_word(t)) ^ r_con[(*indeks) / key_length];
        }
        else if (((*indeks) % key_length) == 4) {
            t = sub_word(t);
        }
        last[3] = key[i+3] ^ t;
        key[i+3] = last[3];

        (*indeks) += 1;
        
    }
    
    if ((*indeks) != 4 && (*indeks) != 156) {
        inv_mix_columns(&last[0]);
    }
}

__device__ void aes_encrypt(const AesParam* ap, char* buff)
{
    uint32_t	state_buf[4];
    uint8_t* state = (uint8_t*)state_buf;
    int		i;

    // input & initialize
    for (i = 0; i < ap->num_block; i++) {
        state_buf[i] = pack((uint8_t*)&buff[i * ap->num_block]);
    }
    add_round_key(state_buf, &ap->enc_key[0]);

    // loop round
    for (i = 1; i < ap->num_round; i++) {
        sub_bytes_shift_rows(state);
        mix_columns_add_round_key(state, &ap->enc_key[i * 4]);
    }

    // final round
    sub_bytes_shift_rows(state);
    add_round_key(state_buf, &ap->enc_key[ap->num_round * 4]);

    // output
    for (i = 0; i < 4; i++) {
        unpack(state_buf[i], (uint8_t*)&buff[i * 4]);
    }
}

__device__ void aes_decrypt( uint32_t * dec_key,int num_round, uint32_t * buff,uint32_t * first)
{
    uint32_t	state_buf[4];
    uint8_t* state = (uint8_t*)state_buf;
    int		i;

    // input & initialize
#pragma unroll
    for (i = 0; i < 4; i++) {
        state_buf[i] = buff[i + 4];
    }
    uint32_t temp = state_buf[1];

    add_round_key(state_buf, &dec_key[num_round * 4]);
    for (i = num_round - 1; i > 0; i--) {

        inv_mix_columns_add_round_key(state, &dec_key[i * 4]);

    }
    
    // final round
    inv_shift_rows_inv_sub_bytes(state);
    add_round_key(state_buf, &dec_key[0]);




    *first = state_buf[0];
}


__device__ void aes_decrypt_v2(uint32_t* out, uint32_t* out_copy, int num_round, uint32_t* buff, uint32_t* first)
{
    uint32_t	state_buf[4];
    uint8_t* state = (uint8_t*)state_buf;
    uint32_t* last = new uint32_t[4];
    int	i;

    // input & initialize
#pragma unroll
    for (i = 0; i < 4; i++) {
        state_buf[i] = buff[i + 4];
    }

    uint32_t indeks = 0;
    for (int j = 0;j <= 38; j++) {
        expand_key_v3(32, 38, out, last, &indeks);
    }
    add_round_key(state_buf, last);

    for (i = num_round - 1; i > 0; i--) {
        indeks = 0;
        for (int j = 0; j < 32; j++) {
            out[j] = out_copy[j];
        }
        for (int j = 0; j <= i; j++) {
            expand_key_v3(32, 38, out, last, &indeks);
        }
        inv_mix_columns_add_round_key(state, &last[0]);

    }

    for (int j = 0; j < 32; j++) {
        out[j] = out_copy[j];
    }
    indeks = 0;
    expand_key_v3(32, 38, out, last, &indeks);
    
    inv_shift_rows_inv_sub_bytes(state);
    add_round_key(state_buf, &last[0]);



    *first = state_buf[0];
    free(last);
}

//__constant__ uint32_t a = 0x67452301, b = 0xEFCDAB89, c = 0x98BADCFE, d = 0x10325476;
__device__ void Evp_BytesToKey(uint32_t * pass, uint32_t * pass_copy, int key_len,int iv_len,int iter,uint32_t salt_1,uint32_t salt_2, uint32_t * out) {

    uint32_t a = 0x67452301, b = 0xEFCDAB89, c = 0x98BADCFE, d = 0x10325476;
    //uint32_t a0 = 0x67452301, b0 = 0xEFCDAB89, c0 = 0x98BADCFE, d0 = 0x10325476;

    //printf("%i\n",10);
    pass_copy[32] = salt_1;
    pass_copy[33] = salt_2;
    pass_copy[34] = 0x80;
    pass_copy[46] = 1088;
    //pass_copy[1] = salt_1;
    //pass_copy[2] = salt_2;
    //pass_copy[3] = 0x80;
    //pass_copy[14] = 96;
    md5HashU(pass_copy, 0, &a, &b, &c, &d);
    md5HashU(pass_copy, 16, &pass_copy[0], &pass_copy[1], &pass_copy[2], &pass_copy[3]);
    md5HashU(pass_copy, 32, &pass_copy[0], &pass_copy[1], &pass_copy[2], &pass_copy[3]);
    pass_copy[4] = 0x80;
    pass_copy[5] = 0;
    pass_copy[6] = 0;
    pass_copy[7] = 0;
    pass_copy[8] = 0;
    pass_copy[9] = 0;
    pass_copy[10] = 0;
    pass_copy[11] = 0;
    pass_copy[12] = 0;
    pass_copy[13] = 0;
    pass_copy[14] = 128;
    //pass_copy[14] = 32;
    pass_copy[15] = 0;

    int i = 1;
    while (i<iter) {
        md5HashU(pass_copy, 0, &a, &b, &c, &d);
        i++;
    }
    #pragma unroll
    for (int i = 0; i < 4; i++) {
        out[i] = ((pass_copy[i] >> 24) & 0xff) |
            ((pass_copy[i] << 8) & 0xff0000) |
            ((pass_copy[i] >> 8) & 0xff00) |
            ((pass_copy[i] << 24) & 0xff000000);
    }
    

    //printf("%x %x %x %x \n", pass_copy[0], pass_copy[1], pass_copy[2], pass_copy[3]);
    int size = 16;
    while (size< key_len+iv_len) {
        for (int i = 0; i < 32; i++) {
            pass_copy[i + 4] = pass[i];
        }
        //pass_copy[5] = salt_1;
        //pass_copy[6] = salt_2;
        //pass_copy[7] = 0x80;
        //pass_copy[14] = 224;
        pass_copy[36] = salt_1;
        pass_copy[37] = salt_2;
        pass_copy[38] = 0x80;
        pass_copy[46] = 1216;
        md5HashU(pass_copy, 0, &a, &b, &c, &d);
        md5HashU(pass_copy, 16, &pass_copy[0], &pass_copy[1], &pass_copy[2], &pass_copy[3]);
        md5HashU(pass_copy, 32, &pass_copy[0], &pass_copy[1], &pass_copy[2], &pass_copy[3]);

        pass_copy[4] = 0x80;
        pass_copy[5] = 0;
        pass_copy[6] = 0;
        pass_copy[7] = 0;
        pass_copy[8] = 0;
        pass_copy[9] = 0;
        pass_copy[10] = 0;
        pass_copy[11] = 0;
        pass_copy[12] = 0;
        pass_copy[13] = 0;
        pass_copy[14] = 128;
        pass_copy[15] = 0;
        //printf("%x %x %x %x\n", a0, b0, c0, d0);
        for (int i = 1; i < iter; i++) {
            md5HashU(pass_copy, 0, &a, &b, &c, &d);

        }
        #pragma unroll
        for (int i = 0; i < 4; i++) {
            out[size/4 + i] = ((pass_copy[i] >> 24) & 0xff) |
                ((pass_copy[i] << 8) & 0xff0000) |
                ((pass_copy[i] >> 8) & 0xff00) |
                ((pass_copy[i] << 24) & 0xff000000);
        }
        //printf("%x %x %x %x %x %x \n", pass_copy[0], pass_copy[1], pass_copy[2], pass_copy[3], pass_copy[4], pass_copy[5]);
        size += 16;
    }
}
//__constant__ unsigned char hex_str[] = "0123456789abcdef";

__constant__ unsigned char chars2[] = "abcdefghijklmnoprstuwxyzqvABCDEFGHIJKLMNOPRSTUWXYZQV";
__constant__ unsigned char chars3[] = "abcdefghijklmnoprstuwxyzqvABCDEFGHIJKLMNOPRSTUWXYZQV1234567890";
__constant__ unsigned char chars4[] = "abcdefghijklmnoprstuwxyzqv1234567890";
__constant__ unsigned char chars5[] = "1234567890";

__device__ void getKey(uint8_t s[16],uint64_t indeks) {
    int id = 0;
    s[0] = chars4[0];
    s[1] = chars4[0];
    s[2] = chars4[0];
    s[3] = chars4[0];
    int length = 37;
    while (indeks>0) {
        s[id] = chars4[indeks % length];
        indeks /= length;
        id++;
    }
}
//Hash w2zz fork tell 9705 call
//cash and swap
//Gate hash wild fire aiia root node lang merk
__device__   char* a1[]=  { "4891","7102","live","drow","txet","emag","tset","onna","inim","dnah","onan","odnu","dnan","dniw","rulb","ecaf","srey","ecnu","nala","nona","2102","2191","4591","9002","1102","8102","9102","7102" };
__device__   char* a2[]=  { "hsah","zz2w","krof","llet","5079","llac","edoc","bilz","pizg","tiwt","tsop","wons","llih","yram","dnal","ngis","kram","nwod","dn24","dmsu","galf","laes","7655","etyb" };
__device__   char* a3[] = { "652a","483a","2384","enim","ogla","krow" };
__device__   char* a4[] = { "hsac","paws","nioc","noci","niag","knar","esab","dnuf","krow" };
__device__   char* a5[] = { "ikol","nacs","tron","nolc","edon","nort","macs","nord" };
__device__   char* a6[] = { "tmsa","xtcc","sfpi""maet","etag","shah","dliw","erif","aiia","toor","edon","gnal","krem","etyb","nede","mada","drow","edoc","efil" };
__device__   char* a7[] = { "tsev","atad","llup","mraf","taef","kcam","loop","galf","ngis" };
__device__   char* a8[] = { "esab","nioc" };

__constant__ unsigned char chars1[] = "abcdeghiklmnoprstuwx";


__device__ void getKeyV3(uint32_t indeks, uint32_t* data,char * a1_file) {

    int length_a1 = 5526;

    char* a = (char *)(&a1_file[(indeks % length_a1) * 4]);
    //printf("%i\n",(indeks % length_a1) * 5);
    //data[0] = ((uint32_t*)(a))[0];
    //indeks /= length_a1;

    /*int length = 36;
    data[0] = (chars4[indeks % length] << 24 & 0xff000000);
    indeks /= length;
    data[0] |= (chars4[indeks % length] << 16 & 0x00ff0000);
    indeks /= length;
    data[0] |= (chars4[indeks % length] << 8 & 0x0000ff00);
    indeks /= length;
    data[0] |= (chars4[indeks % length] & 0x000000ff);
    indeks /= length; */

    data[0] = ((uint32_t*)a1[indeks % 2])[0];
    indeks /= 2;
    //////////////

   // int length = 36;
   // data[1] = (chars4[indeks % length] << 24 & 0xff000000);
   // indeks /= length;
   // data[1] |= (chars4[indeks % length] << 16 & 0x00ff0000);
    //indeks /= length;
    //data[1] |= (chars4[indeks % length] << 8 & 0x0000ff00);
    //indeks /= length;
    //data[1] |= (chars4[indeks % length] & 0x000000ff);
    //indeks /= length;
    //data[1] = ((uint32_t*)a2[indeks % 23])[0];
    //indeks /= 23;
    data[1] = ((uint32_t*)(a))[0];
    indeks /= length_a1;
    data[2] = ((uint32_t*)a3[indeks % 3])[0];
    indeks /= 3;

    data[3] = ((uint32_t*)a4[indeks % 2])[0];
    indeks /= 2;

    data[4] = ((uint32_t*)a5[indeks % 1])[0];
    indeks /= 1;
    //////////////
    data[5] = ((uint32_t*)a6[indeks % 18])[0];
    indeks /= 18;

    data[6] = ((uint32_t*)a7[indeks % 1])[0];
    indeks /= 1;
    //a = (char*)(&a1_file[(indeks % length_a1) * 4]);
    //data[6] = ((uint32_t*)(a))[0];
    //indeks /= length_a1;

    data[7] = ((uint32_t*)a8[indeks % 1])[0];
    indeks /= 1;


}
__device__ void getKeyV2( uint32_t indeks, uint32_t* data) {
    
    /*int length = 20;

    data[0] = (chars1[indeks % length] << 24 & 0xff000000);
    indeks /= length;
    data[0] |= (chars1[indeks % length] << 16 & 0x00ff0000);
    indeks /= length;
    data[0] |= (chars1[indeks % length] << 8 & 0x0000ff00);
    indeks /= length;
    data[0] |= (chars1[indeks % length] & 0x000000ff);
    indeks /= length;*/
    data[0] = ((uint32_t*)a1[indeks % 26])[0];
    indeks /= 26;

    data[1] = ((uint32_t*)a2[indeks % 18])[0];
    indeks /= 18;

    data[2] = ((uint32_t*)a3[indeks % 4])[0];
    indeks /= 4;

    data[3] = ((uint32_t*)a4[indeks % 7])[0];
    indeks /= 7;

    data[4] = ((uint32_t*)a5[indeks % 7])[0];
    indeks /= 7;

    data[5] = ((uint32_t*)a6[indeks % 4])[0];
    indeks /= 4;

    data[6] = ((uint32_t*)a7[indeks % 9])[0];
    indeks /= 9;

    data[7] = ((uint32_t*)a8[indeks % 2])[0];
    indeks /= 2;
    

}

__forceinline__ __device__ void useAlphabet(char * alphabet,uint32_t * length,uint32_t *data, uint32_t * indeks) {
    *data = (alphabet[(*indeks)%(*length)] << 24 & 0xff000000);
    (*indeks) /= (*length);
    *data = (alphabet[(*indeks)%(*length)] << 16 & 0x00ff0000);
    (*indeks) /= (*length);
    *data = (alphabet[(*indeks)%(*length)] << 8 & 0x0000ff00);
    (*indeks) /= (*length);
    *data = (alphabet[(*indeks)%(*length)] & 0x000000ff);
    (*indeks) /= (*length);
}
__forceinline__ __device__ void getKeyV4(uint32_t indeks, uint32_t* version, uint32_t * data, uint32_t * lenghts,
char * w1 = nullptr,char * w2 = nullptr, char * w3 = nullptr,char * w4 = nullptr,
char * w5 = nullptr,char * w6 = nullptr, char * w7 = nullptr,char * w8 = nullptr,
char * a1 = nullptr,char * a2 = nullptr, char * a3 = nullptr,char * a4 = nullptr,
char * a5 = nullptr,char * a6 = nullptr, char * a7 = nullptr,char * a8 = nullptr) {

    if ((*version) & 0x0000000f) {
        useAlphabet(a1,&lenghts[0],&data[0],&indeks);
    }
    else {
        data[0] = ((uint32_t*)w1[indeks % lenghts[0]])[0];
        indeks /= lenghts[0];
    }

    if ((*version) & 0x000000f0) {
        useAlphabet(a2, &lenghts[1], &data[1], &indeks);
    }
    else {
        data[1] = ((uint32_t*)w2[indeks % lenghts[1]])[0];
        indeks /= lenghts[1];
    }

    if ((*version) & 0x00000f00) {
        useAlphabet(a3, &lenghts[2], &data[2], &indeks);
    }
    else {
        data[2] = ((uint32_t*)w3[indeks % lenghts[2]])[0];
        indeks /= lenghts[2];
    }

    if ((*version) & 0x0000f000) {
        useAlphabet(a4, &lenghts[3], &data[3], &indeks);
    }
    else {
        data[3] = ((uint32_t*)w4[indeks % lenghts[3]])[0];
        indeks /= lenghts[3];
    }
    //////////////////////

    if ((*version) & 0x000f0000) {
        useAlphabet(a5, &lenghts[4], &data[4], &indeks);
    }
    else {
        data[4] = ((uint32_t*)w5[indeks % lenghts[4]])[0];
        indeks /= lenghts[4];
    }

    if ((*version) & 0x00f00000) {
        useAlphabet(a6, &lenghts[5], &data[5], &indeks);
    }
    else {
        data[5] = ((uint32_t*)w6[indeks % lenghts[5]])[0];
        indeks /= lenghts[5];
    }

    if ((*version) & 0x0f000000) {
        useAlphabet(a7, &lenghts[6], &data[6], &indeks);
    }
    else {
        data[6] = ((uint32_t*)w7[indeks % lenghts[6]])[0];
        indeks /= lenghts[6];
    }

    if ((*version) & 0xf0000000) {
        useAlphabet(a8, &lenghts[7], &data[7], &indeks);
    }
    else {
        data[7] = ((uint32_t*)w8[indeks % lenghts[7]])[0];
        indeks /= lenghts[7];
    }


}
__constant__ unsigned char hex_str[] = "0123456789abcdef";
__global__ void decrypt(uint32_t * buff,int start,bool * success,char * a1) {
    int indeks = blockDim.x * blockIdx.x + threadIdx.x;

    //std::string a = (std::string)((&a1[0 * 5]));
    //char* b[] = a1;
    //printf((&a1[1 * 5]));

    uint32_t* data = new uint32_t[32];
    for (int i = 0; i < 32; i++) {
        data[i] = 0;
    }

    //31313131 3a35314e 66673336 382b3936 3933383d 39373030 36

    
   // data[0] = 0x61616161;
   // data[1] = 0x80000000;
    /*data[0] = 0x31313131;
    data[1] = 0x3a35314e;
    data[2] = 0x66673336;
    data[3] = 0x382b3936;
    data[4] = 0x3933383d;
    data[5] = 0x39373030;
    data[6] = 0x36800000;
    data[31] = 200;*/
    
    

    // 35373130 3a303131 38393044 65616453 65614264 36417070 6c653033 39

    getKeyV3(start+ indeks,data, a1);
    data[8] = 0x80000000;
    data[31] = 256;
    //data[0] = 0x35373130;

    /*
    data[0] = (password[0] << 24 & 0xff000000) |
        (password[1] << 16 & 0x00ff0000) |
        (password[2] << 8 & 0x0000ff00) |
        (password[3] & 0x000000ff);
        */

    //616e6e6f 636f6465 61323536 72617465 7363616e 62797465 64617465 62617365
    //anno code a256 rate scan byte date base

    /*
    data[1] = 0x636f6465;
    data[2] = 0x61323536;
    data[3] = 0x72617465;
    data[4] = 0x7363616e;
    data[5] = 0x62797465;
    data[6] = 0x64617465;
    data[7] = 0x62617365;
    data[8] = 0x80000000;
    */

    /*
    data[1] = 0x3a303131;
    data[2] = 0x38393044;
    data[3] = 0x65616453;

    data[4] = 0x65614264;
    data[5] = 0x36417070;
    data[6] = 0x6c653033;
    data[7] = 0x39800000;
    

    data[31] = 256;
    */

    sha512(data);
    //data[16] = 0x80000000;
    //data[31] = 512;

    for (int i = 1; i <= 11512; i++) {
        //sha512(data);
        sha512Faster(data);
    }

    //uint32_t salt_1 = 0x0c3d6cac;
    //uint32_t salt_2 = 0x9ddcb399;
    // 8521dead 6df437dd 
    //uint32_t salt_1 = 0x22030f1f;
    //uint32_t salt_2 = 0xaaafbd8a;

    //7cb04689 45c5da62
    ////uint32_t salt_1 = 0x8946b07c;
    ////uint32_t salt_2 = 0x62dac545;
    //////////////////// 1f0f0322 8abdafaa 
    uint32_t salt_1 = 0x22030f1f;
    uint32_t salt_2 = 0xaaafbd8a;




    //uint32_t salt_1 = 0xadde2185;
    //uint32_t salt_2 = 0xdd37f46d;
    //uint32_t salt_1 = 0x8521dead;
    //uint32_t salt_2 = 0x6df437dd;

    uint32_t* data_1 = new uint32_t[32];
    
    for (int i = 0; i < 32; i++)
    {
        data_1[i] = 0;
    }

    for (int i = 0; i < 16; i++) {
        data_1[2 * i] |= hex_str[(data[i] >> 28) & 0x0F];
        data_1[2 * i] |= hex_str[(data[i] >> 24) & 0x0F] << 8;
        data_1[2 * i] |= hex_str[(data[i] >> 20) & 0x0F] << 16;
        data_1[2 * i] |= hex_str[(data[i] >> 16) & 0x0F] << 24;

        data_1[2 * i + 1] |= hex_str[(data[i] >> 12) & 0x0F];
        data_1[2 * i + 1] |= hex_str[(data[i] >> 8) & 0x0F] << 8;
        data_1[2 * i + 1] |= hex_str[(data[i] >> 4) & 0x0F] << 16;
        data_1[2 * i + 1] |= hex_str[(data[i]) & 0x0F] << 24;
    }

    free(data);
    uint32_t* data_2 = new uint32_t[48];
    for (int i = 0; i < 32; i++) {
        // printf("%08x\n", data_1[i]);
        data_2[i] = data_1[i];
    }
    for (int i = 32; i < 48; i++) {
        // printf("%08x\n", data_1[i]);
        data_2[i] = 0;
    }

    //for (int i = 0; i < 48; i++) {
    //    data_2[i] = data_1[i];
    //}
    
    uint32_t* out = new uint32_t[36];
    Evp_BytesToKey(data_1, data_2, 128, 16, 10000, salt_1, salt_2, out);
    free(data_1);
    free(data_2);
    
    uint32_t* dec_key = new uint32_t[156];


    expand_key_v2(dec_key, 32, 38, out);


    uint32_t first;


    //uint32_t* out_copy = new uint32_t[32];
    //for (int i = 0; i < 32; i++) {
    //    out_copy[i] = out[i];
    //}
    //aes_decrypt_v2(out, out_copy, 38, buff,&first);
    aes_decrypt(dec_key, 38, buff,&first);

    if ((first ^ out[32]) == (uint32_t)0x7b226b74) {
        *success = true;
    }
    free(out);
    //free(out_copy);
    free(dec_key);
   
}

cudaError_t findKey(int key_size,int key_block, int num_round,int iter);
int main(int argc, char* argv[])
{
    uint32_t version



    cudaError_t cudaStatus = findKey();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "find key failed!");
        return 1;
    }

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;
}

void readFiles(uint32_t * length,char *& words, char * fileName) {
    *length = 0;
    std::fstream file;
    file.open(fileName, std::ios::in);
    if (!file) {
        printf("File %s not opened!\n",fileName);
    }
    else {
        std::string word;
        while (file >> word)
        {
            (*length)++;
        }
        file.close();
        file.open(fileName, std::ios::in);


        if (!file) {
            printf("File %s not opened!\n", fileName);
        }
        else {
            words = new char[(*length)*4];
            int L = 0;
            std::string word;
            while (file >> word)
            {
                
                std::transform(word.begin(), word.end(), word.begin(), ::tolower);
                std::reverse(word.begin(), word.end());
                memcpy((void*)((&words[L * 4])), word.c_str(),4);
                //std::cout << (*length) << std::endl;
                L++;
            }
            file.close();
        }
    }
}
cudaError_t findKey() {
    //std::string base64 = "U2FsdGVkX18fDwMiir2vqpWNLgbPWRSfUTF46w0Bd8DI5e4m2pOdUXScDSuq4Epko3EMrd5LO9qvu1Y7JQGFN+QAUHpmHKttOu/mSzXLobfSqzyuYuU0YFvHN+I1ldufP2bilXaKzW8c4w2/a1FOakMYK59C4J/xTijgo3jX3Utr2zP1gMmryz5o6uU4SghsMrhJ3trFua/e3dsLmXpWjvka/4Q0+na8OVQzZuxyb7dwcLM2SC+SVO9wye6A5gTha8uQjkUPNsKMaN+JlJ1HrUyEGOVm4dHLjE3qp79oz/JH3WzJggls5MulW2pH+zojmdQGoO8MwbCQXI+SfBvOEZOfsGsdTeKu+8H3ILUv2GuJoEjpf0d5+WBMrHHPirhQ4bDB1FsiU757kaiB/nHnULLYF+ks9tL9ZGFTxqw0Gj3d25JKeRvndkrrKxgNkf1LTRpcfZcQCzXr0QB2kuEi5Q7mQTAs3nY8D1kURBXkE1BAT9Yf7qH1iTU7bUMYeLh9ev2QNlRnDLO5uXuNY3aCnBnSec8FMV3emoq9I5RNPX0JZij73PVHa0ZaHDS1huUTVCK29RaAr6fdSu2wBC3imc0FxoDregIohzUnPDV2xl7TzIIFYQGF6Etd5Vg9UCAhURK13u8uP+8BWEx3MfZ4Hkv5UOBZ3mM6qIhI/tWu6zy+2BwwLeMWRd7B4CUB/HWpSCXFcEXR8tnzZfelXurcbgA1Hw/K61b+dbrxkCVhbwsyUtVqWZugjO4kK/mmAvfFLNZ+KWtAyMSaX1zbOk5zK3lHwbNtQwKQNu7Yoe7IcGnyGS/DB6ra5+rCyM8DTdnH0VJcY9oSoIYwjVw+3wluL+ZGUTMU6IkAzEoSgn4m6DxxrvLTTaAywjCtbXM0oXYAkOgMn37HnFJ6zZz6qTQu/pJOMlpuQMPOCsu//YtdWlQx68JwPi1SpYz8xoMIW7v8sF1dKAyLWmiaQq6dGQR4D9B+7jR3Pi53jZZkVgCQUhPwwE8zvMac/IfNNqvnmtqoRn2NI9gLzJgwOdrVA3z0UGZzwlhC1tkiK/zfYZVofbSkSV4vEeArMQPZHZ6k+lsG0QSRSm/wEs1TlJL7aBAnwZbpRmUR2XTHTtWruz57l+K/Y3fKSdIZTHAURUheDA6QhsHQf97t0kW7Oh1TMQmoT9rI3JtBSS6DtvVE2oMfIpq6bZFOGNOdrFippiO6jrAAHOJPQrp6Pr2oes5vZzFNfgetLv2tDZyJf+M9YrTN2FzMYoEb+yhD1UM4LTTMeenguG4N6XbWF+qOBjbjyBdvUkCMQuaIWM60s7fc8GDRGLk3HlZt99Z0Jmw9GbBfC8kjIcE7fKZRBt3/ymZLsTtliLA207ue1vP+nXNpdizHrCFdAqm1tUkkIJN6IOlgtm3TwopPtB6wJpQh5ASsj8eW9yZd4dkrvJ9W4pQLzWG+p9+ANYBHj//VkInVyO/N+Cl0BpfY3Jf0vqAQua+q6AvPBPTaIndb8l3M9B2EKWm6R4aHrCJ8UdioBeYD/atVn8nc59RP4AS1iHJCksCl3MezRtgam5JsOes/f5X+V4DyGCyPOwq9qfkQYn4FgYBrCGw+AY/V0JXZwSc9rK9VI6/C5ujjJm/ytDTcXdlapJhg4TljLFSn59POZwiddNvZEtimED2/gWneoV9easD5qaHY666/VuaSgx/jegwsNYmZvbKZ1/ljPxpd9LKhYGlRtveL61y9maHuxmT8XURm5ZOLrA51Q8IJdx6oJ+cJgE7KnL3EBq7Ig627TV23VGSuxLaniO8iWZuBKWh2NfJ8z4n0gyLr7iFaULag3Jq4RdO2KVummYG8qLVhKTCfGmuKNMvwmfkKefQeChc4l0nebyCKNPJvjDne7hvT2EULpHO9z+FUz/U5LRDVxTOKxBdxDomZSZzDWpMhR+1VBr+nOzLExZxaEs5FCgpmie+HGp4pqdSvU9h35Yaqo6vcK1XvwPyRfUCr+K4fF52k4eH5xEsypvRyLbwQqAcjup3cfoWATSx4rWcQlkkX6UNEUZ5rBhfiGeOD+l8iSS26Npx0u1k+Y9fzXYkZg1Xhpd62PimB9ETMy4MqnbM61q4qPT7rly9N9zik31Yfea7QxgqMs6am8/c6Qt2OofHsvVzctJE8RrHMrBzSeLgPTc4wNnYeeFzcCkXQtUb89EH6D/lTVEVjOianzdFOj4ZRczN1DdbM3kuUVoyXOv3P+4cA5qr12mlfsV+7pnFwLQYDJqgK8y3iDPUvDBLQG8Yi3PSu7mcwGagwlo9sP3zFSTLIvKQUaQvYN9U49K9Uk43ZvLYQTrunZVRVN7eFvnfuUSguLZlCUp2lHQOljWmo470B8Hdck9poo4Fl6QuMn7JRkg7QiuOJOLXWI8843BGz8wdkPN31zt0lmZcfx5d0EEnCTVd0THhu5S76JtpSR7i3/tH9Mc4mYx15WP8/6HRAaZNAYIUJlAxgmtlB/TCsnHt01sDQJu2b0+aW/W2MqhQZgnVSaXWBSkCURykBBp/s9x7gV3rPFR0umYgMeQaY+MbKqf7nruzgLDESOpO6FhHDPsnvUf0ROEDcLK5uVKswLTxkD50Nk6RcJDR18TKB+FdZIfE9ZoUnmbGOl5lcxvvdCUssRsbTRlNO6hjQNSxlRnK5yvjcT5lUgCg/Cw5XjypgvstV+g+PqlrC+Gnnw5YxKN82qXBIWzyvIqmsmCuxl01kTu1z5KFbPgOg9nEJJLvuraaeyeiqRpWlcB9ujZJx+kQ04GUJnt8HLhIdCiV7r6oYpDh4TxLMm1UrsV0IxK2kuJNwwuU/BRzEFjiFtdiQkJP2LU0rEcOAo/0x2P9FvQQLMjGyJ5fgr2PgCm9/QyxCwyUNLFhGUasQUDwS8UpOjHaf3bbFeHjLTyKTbpgGlA+kT77bbMtmxVih43RqWr62GS2qrBexAeZXMCR5RM0rg4L+N8197z9zYaF4VZwtlzhXk/fb1vIKWcI45hWIieRLtv8DFu4qNZlMmstwZBpA7acpmGN9wRL+gp9bLRFFoDcAWrRolp8kPCLv2u0/objFfjRQ1oGwh815JQwbezdz5Dt5uTesgC2x0L3gbldHMbnP7zRPZXbgN8AmToKc1jZ7ADK8frTC9HCAXB7v/yTdqAE9BhRbhBbtsSt0IX178w/iKEkInLK7aJOZIDsTZuLuDuuOj0sROYi2Nwl6GPfeM+yibYKp2/s8hd5XPl6XThfkVczxuHZgJjktSCX03JaVsgCfTEj5vOnM1xYZYlTQtWMQElNCw17TKxvWaDbOb0W/Izut7m+zkQwQEDKMuVtLytZG6EmMExhYNimyFU1BBNWpgc8OlC2BebkEReLHOxEMDdolqrKVZ/ge9EAGCj/PtJ+RLRp5Eq3RKLKBH+h7J1Q/Crc9KaXriRsBoOXe+MhTt8pSIeJvkZicFD+dr61HZYJSnJnAj+HZdOlpeBAqewgUbQX3gVG21b4AVf0njcoSTHexBqw0jgjiVwyCiL4SRw4c/HR6uIztx4VIxcrcWHLxutKJM7lz+T7oexVYyB1XdZFP876oC1eb0uvdCSqzPhltDu7ALxLdGybX2DSdgqpfYXfsbE727Kbr7ztQ5GDSEMA6mOU6zE/1I8O4Jo0wQfikvmGJaJmRVFj6IdT1q9QH3+PCDFgB17eI4maMdg26RJ75ePmF+rcbr5GroRiktWnchy1o5UBF9xYnxWWBF1UH62oP5xEMmld+6ejRTvqPvIY4Ohq1bMFylramtNlHfDcKugKKTVBPbNmWDYYYDo71qnNNNFPJnYRekQAF00VpA2EJjP+9H7ZArCQkaB0+1wxJMiCqi7an+75PNNgocX6OcOsx1+ip6G6hC9F/yx/YWgZ6bU2Uoqmf2+0ayooXcK/WcVFnDh+/S2lSq8hUiPVe7b5EBQ6sHvSicE83+BTXwoXj2QAE1+HKDA66/mJKP19FWw5iJ4012EvRPofNSfdyXnZtoegglK5oGbLw3nfr17J5TT9yH21uBmRXKohH3mWPqrgQU3TyngVm+cMFHv9sJyRq1Rqj4bwPUvmJk2C1LiWLBsehvqGKjcX2yPDYgbK13mFSvjy0bYpmb/5p3x5K8xMiOq3DlWd1m3Ohn3h1SEAvwYl3h2fsokJIWmG9B8uX1jMh4PemsEC4YwiV743Q2o64Z1zvHwzvo7oUTEUi6TCxavd7IEE9iE2+8n+gDU2Rj0ygc0eaGq7jxeIiq5T4lKmbVQ==";
    //std::string base64 = "U2FsdGVkX1+FId6tbfQ33ek3LOMowmUoXk+AptiggH8CttedAnAntKh69Ub8JEowOgrzP1Z/vDqqNVI4cnfLlcFZ8B8iP6c8Pjvnqt/bWuY6Kb04HANowO1ICYulZOvtuuVl0otBU6DptpHy1jpXAXWuyAh9tNxQqLWTDaE75h2O3w9ntP7QQajeszxWZJlMg30lkKG8Ci5KdCe+1DNZ4zkXuq9iUSXs9Hf6jOGnRPponoBSi9ykuVS9NQA8kykWHTZbpKG7XNClr6L57q3C06TAIYxrdI0f42ZAAcQQHj1aQ0AgF844U66V/DJIRlxeFgKAdhc7gs/LR2IZDfEeFR8vGuvxBCIlo/HCk5eK90V3DIZ/IAZUGqQX3w6aIwhIYvlKVBCiCNxRtpCy/N9B2/uPwAq+ka/KSn1RkQ84cnCgOdDfkmW5J4FU+ZQaxxLboxOnEWQIX9KcsS/TpvHQ7nUuYvhP9d/nze4QK4Sb+73Ahm0NrAJ4iCzHwEUXAXVNys3Ww4Nc91I+4TpRo7o+G/SFFeG2BsXj58RLpDi5zjvH5yS4AYOn+kcrZ/RuPfhm2QNX5VZw/aNsXAN0FvpCV7tnRm3zg/MbX7bybEADSN90CtR+tSwh1mylUaR3Qv38I/SNicxGd904sYzWFl30BZ1ycEMxId57NNMo637v+XVHaoPkDkKAfCzebQjQ10d8m25bMbRivam1PKsg+Sw4rYoFdKBqXJUygrWupY0G3tPzXbVyMTD23syDYF93u3V0Nj9FkUz89qKjeqxGhTUXQqg2Ou335hgZRGBZvZri67yuI3zM70qCeDhYdo/cm/JqayVer/6z61vv3+AQPKmjvuuUlPEVlHDOTdmt24thGtl8fvUl6jCuD+w5xnElp7dV6MDtmBoXz7zBxa8OyZhn06bWNvRD/GsuXkMY3rFK6/Owqr4Hp83Kp491JHRufcgI5X3aQJcvWw/8hLhEQ2I7YboDUiDwVbxdR7iV4HV2uatt2IKJ/RCXsVRk4pynTxg2E280P9nw/op6kr5ry1tz3XjoyuVPfp9eH2PBYrkKGwVNsTmCW6Nw6GBwU5EUeaDXBxbtDclPH1WBaDnIUZ+iOrJ90qhhosOB4EEQZNIQGwzmkbwvH2V90Vd/jQaGhUZl4/UGF1g0vzFXjsFmwiBxHbxYD4wiT85o9akILXk76PJwdGneXmny3LE0lni4vBtMoYc2v82rPJu0f+9JsYC0j7aF+FegXF8TouUEapPA3G8xC8d+/9HX/poMw+3VgfYF2qCd/L7S4650/WbuevS4eTjpI+aH0E5HhoZxEaD2cAjq0TRCOtXy3iuVcWTIyGXURT84t6bqzjf02vwuBXokG74UO8q3Bz6PZE+kwEn4kAOT3W7+bLIB2GHpy3BWSSgTQIWvsDEZRVinN8HkQ6VuUoTnpCq/pGHWP0ci+Jd3DDXqp4n2cZsC4ES7WAHyvcBETIbpyJt4IxT9ktrTo/iIAX17m8ZoOGvOoaJkDVFrFUvDsNfKMm/UYpPcX484hSw2O5bsxNuenC9JTwSOnb+PzSr8Ed9iPhmt+I4m9JZ+EzJc7H0HhEIZA3x4cPNsIIK3BDBAbtYIm8m7L3wNz8lH5mV8acN31mHRZ3w64VaTaMuxwFeW496afIU9KSMnKMzMVtTiNpmmIy8tnRSOmR+CDsMMIoYaD+X0FWa47O/2BDz8GWT4rnbVOo7fa/OdYA9tyfsUOoP+YU0pj1I+zKpBRg3w1EmIOjD6zDmLhxdr+95ORyK474/O02uTwd/Bp+H0J2dU3PhIb8cP0rd86GgQ2bVgqVtphbiENclNTqxofavwHnVVk52p1tea1P5RrREESyEvAz1wBFDP70oy+eAgYzDAezd/++Gs9BRF8BwveIfk3wAoKO8GbrVM4QG6AflpAZBuMjzM453PstBjTapWzHKDanehMFVvr+K5yjJOHfNJJnap3O1C1Z1irQxiYt9l2Wbr8kV+Sqb3fXYoX/1Jls5idBpj1M56IIGNJmYw2iM11keF0W9Ct0zAU52tJBtUmgYhx7muFQnDggUzwt6/jl6nW6rPFKGKfSH213JtipLOKfcKKHQZjNj7wjAvybhClbN81zYQci70WLK/5v/TsHffWTudfwDCM04gJtyToLWpGDFup5QYyUP8v9c+rdT8v3SHrryer1h5N5Yinp/SJkV5rXdjM+ze9RCChmz33jAJKKJ5JdVycsKHD3l/0EKzQV4W81OXSnmgHMaGEf5AO13YDtTYbCWKuGUjI4i8UxtvOG4nylnjMyxWnUvnDJDsskFy5iHu2mmHu9Il88ifaP33+dU/sIR72HGpl2wQORM7fslUpxesLF1TsM3VCp70KgkOsdPb/Ey1LKDC2vecLa/7uC4j5lpnx8Bql/1l4e8S+ZE9ee21KDF5bK5iAIySjaAd9EkPW9pMH3jA8tiJtjuLUdXGxXHG+Yw/UwrNbaERa0LAgJyMHyclV8faj+mU3dM5rbNOlHInvKMIlxg571HxW4YsELG+tUWUXmGWV7Z8yUEO3hI2wYDgp50vMmE15IcyPE2MERgca7cY90BmrcBn6pYVuNMepy1UwEubR9nlHDgPjHhPoT86O9BxI8Nwh2bRdC5K+CylsGHYw+JmsIMqsW/aJBcbv+FpV4tlYRYQABMQvDgbFqNIrNV4a4ZzBdR96OHccGKak7qmH62kPVD5FfZMaMv5dlbLLO9pFhyKDtzjzBnR/TsX8K4cOS0q1NQSp7V3jf9AG6v2ZaR6U5OO/9HeALEJLApxuqYHNMW3mAKfDoOaYEkp2gHUOAWLLYsNjQSytZGbqjxCEnW3GPN/TKMuhjiJbU8PvqtNj4NbTp6l6RaN4ed5y8UFbs5/0KQG9DyT3nX90fCZ1PZCMKkLT8A2sDn2t7XLauSpIetcHK4GuyWfeoPzKyARUhLdoygvw4L6QTipuXdYQf43rSPWkbnwnJj66Xsi3umF9Pd4oT1PTEygCy6N/vEpNSIvbaMelLfHZ7Pt0Oi2lFKiqKvTFSFANLd5oL/Ah1+hyDxZ3bieLWIEwiGt5LcWSor1+iLl2HT8zGfXyMbRz8aL5rt4vQqe9fNdlkzFM/mzcjyESEaGJC9mhoH9aNWHFb5gg+s76I41QpH3g1oFN80jG+OFrGec61LaH3e11XeD0BPL1nxMe9DiIEb5cHnHXc4k4GCN6Sgio8p2nWvOwCj8UrucS+2ytwlsdrb6iTzvtmcMFN29p3awxQyFq8woWpriADyKsCbDIyJ4B2BIj3vUB17ryFRUd9QTUPz7wxj24Wv14ddoEC+1vQGCbsSWECu3rMn1kfAXSEWsqzFf4vklvmNfS/0jOSvGracSEdk5IVLy3hl+qcybNhedo5eNTCmcyvdEVCV7z0zNoqyRgojmNMsgFD1uwii9xENTFdP8sep5OeQJyiTQ0yP9Mbqb3ukhFk81KrXCq1/qeMpNH5drhOreZsmSKGW18yTyRFowQX0/0trxJHPiM8bCZAbW4hf8Ey5EZukxlJMPJlhcICa1JyVmxF9ms5B1vAnpLdNtvLF6TDtrlRI6WfINkr32OlHg/rYgRAIYkXlo23yw0x0YXzeSDTtPJPi74qjzPCz7s3Zib5TnrD5CSoAEEY4R8wp5/cOnPfFSn4LOGfYXdo3N1rEU6p/RrajKiBrxogi1FS7KOtLI1PQsL8zkc07EYdYqkb1IiYc1Ao85d8Af7fjfe0LdsscNjdyViJ6LmthEYxeF/TSFYRRkl3+zHrWPbFtI2UFXK5knw68uXdZizDfQqwIDhDwH33oG/zNtRZjNqLXwz0T/tMTlObIezcbRR2qbHgALbra1lQiLup+T9K3jp/q11TQvJqEw2egen7LOoiOnV3eyQZy3AHXm5NrZV64qwSTtEKyiNWzmvIpdNSqiChW717eqcLLHVM9azHLBf8q6J/97P9mUrza6sv9WMXvbwQ2H9sUcwdXZ522/0Ul+7Vp856rm83BrCNpBqudZtANdAml4BTPGWAihXsHqZeJPQUd5ihi/j7F8e2w/CasV3yLMXh57NlqUEK6ckCxIL4s+ss385xSE2M09kMWMJEU1sDxU+jOJs7iWjJPPNomDphW/7tOTdHjxrZZf+S1v7oYMfPB768SZ+gkOcDFIybHBYlwCHM4qWPE2UGETGp2WAetrxrd9Ml535xKjrmM7+Zd/ottHzLXXkvfRSeefvRzPCqgQl05ZFjFi/ONMTScTcA==";
    //std::string base64 = "U2FsdGVkX198sEaJRcXaYj74/Wt1qw2tcmf5aSfr5YCNnuKz7ys3+VP/QSeF+8z4ngk36IS4pkVRSzjxq7GgrIEm/1Igh7/lluddtYpzxsUbOrQHxpmPv9cQzkcod8IjAQo4BIRSiLVDQE9wECUuG0DItRkum0gb1puM0EgAqnFv/dkE7RkqPlhyErCgHtZt/fuyGv/xCODiZs14/W/rS2wj4cgM0afUdv/xAkMox7iiSvF5u+5uZcwSkl6kSATovWgHsdoK8yJVNlkqAb5RAt2WhYGxVEOc80Ju/iAB2caq72S8FybBVlwohTjTHhYcnmye3GjpAJZ9t9gsAh0C4PhEpebGOSMp34wchsK7Oxj2Jkxw4NCvKt4INsFMp2xFyoHvVf4ro3WjRKkpLbU9ci4yOsPN1LmaWd6PY9sSn9E3ixWlW8FkQQ6fp6LWYuCFqrdqzv+cumKeIuHf88+POfed6L1xeDCnmjuqsJ3x5YkNuJrMh2O3f4ZRQEBY9rAnxt/Y2/F0T6VXMODMN1Y+kxwbDB0wORBXvUUvrkwr9ACo/t5N3nDpQhYBKL4ZVjTpSqhLLd8IkBFw3LWZXr+TW8xGWWkm0I9xVVlMegNV2bN2BS3eM9LgJj3C0/WbBiamp2ZkHrWuoTOpfZZoyKaCQsrPxW5pyhMxY+9Lap6HXUANxo5VMUxlX+jjo0kop/Hbmw+Mj9FxuZ8NRHR1mc9aML/Lt6UM0/5u2RrqaZlQIdXtQXCuLfb4s4dG6eTp0R2dtRiLsqJg9+NzUjZF/zzrzwBXFL+jSpK5fu8bq6/BQYzx++GJxGH+u3YpXlxubnZRfZML4E4vjjxHzkgMo/gdP1+jHlAcsWsDHHWPLDfz0TvZZWjMjt5xO8mkmZkLVF742kLonhDAZhwI/UpZU9hVq6R/zri8XglA+uxsmQaM1EKaZ6KfJam/YBRiDhoMiooAVyt/Bu20EzP0uQrbfaUcQOQrNCEkIokKp5V7kYFFyyWmtZjiKPSMqNbMBbUVSnuCkevjVJkVOPbalHPBRc1InP60oyFVYvuEDcpExHpCGrXFZOaVJ7IXM3j1t3ykx+ePd1rllJ0dqOe6xMkvXHZSdk46WgvtPsG7cY1CnPeInmwyLVdBiI6SjVTtTd/a66hT9h2VwHaqa6DJeZ/jzaFctILYqSDXP5RKYxKbbn25Bhfk3ZR/SbgJFgveRmir5HpNqcx2LYNhjmuk1DlbVNNmED7IYssXUY1zhLZ0r1Ly54JVnzQIHZu4uMO8e35IfaBaHDf7xK08AqS20Ek3P6nP943C85Yl37jyXODvD9mTwI1HjFWoRvV0OzVTCfspn1aJEbH7POxTsnssHVSGO9bucBSzoKA/tkQnefnOEBS0FtRxr2tvvbVtXlKXnwVV9F/4KJhZ2BjkASN8YxFYHi2oMR4r3qEGV+HHO4oGu/ioJfpLNDMiS2WJ4POhRuG1SDItepg5/6meMUULpMlVRx/ZcSCqB2TjhxO8EI8NucyfVQ36M0JRBb4n8s7QAUzfIOcJPpqeIl82o30BR7D+9b5gF4aFR+2p/LnwZEeuDxhgl6V2h30Hoy7t1NtbH5eepGx+1CQAIg31PJxJZUeT84mf458Hjgr2b5b1JLMskcPThyRGNqXE4odsMzqTtA5VlagVmMvmtGlK/1FAzHQzoTIvlSe/MpeOkP9pnr1JAy6n2Cvxy/468DelndAba2FqEEfY7Q/aHcBHWd258g8USBhabUFZ9zhsddyTcTvQHynt0VnhWa0Ea6DJhG8OJL5HmbQmbbcB0+hGA8BitkXHqjJE4U3CP+lfMAu7Z1OJaPNDPe4lK8SmjfHUrSeT7a1JXwbrclTnUFhz1RrdnCH61AJOhq6xLQYfYDudzAaVLmB7bEChjaAi3A5K7I8Jk/mqRrEm/e9Y1/pBLZ3sQ6kVwZYrh0GxrHV/+5Z72bQTzDtqaBYNCRuVpSsszLYdGr6Seez3uShrOrMwOF95BxVh6hyNg2ztwFvXkazDJidvzoPOTKgdgvDVVmyJkoH4KJwIxfnqee+Ma+qU1SHBc+qGT0ATOKULjeLw/3lhyufC0oKoyGQ5m5SCTPKas/opD4RMIlSLbRjVh2+xqK1zbEPVR5893HP5SrUSGDWSDgFW3kxPatm+e8C16wpgu/mcqAd7D5EbPuNhLXlBJpD5yRb+az+eyBeNwJEsS+l68Qm03qd/WJFYdZOUfJmq/XtbppBQcihWBzZQkS/mzJHjCAWZHuu0PhLcYyJg4DwEMKMqzDQAqR72XBo8F5kO4Q1ctIioVn7C4Evtnhl0qpT47T5f4jkSqbcVYVrLGwUQz1aWVP3NCFNqrs2gTlJrZ4lvyhoxm39MEJPgyq5aEO0yZReC1vuZZxXVteHmQT+GX8fE8wc7Hsqo0/7DqZtn8GLVO+UVrHMFFmiYc7gE3fc/zWKp6oceFSlAVFFh7j+NmIucze0Lh0g9NJTTwQf/rg2flDpgMZPbl0rUVzkhC5paI9FNHtjESmTh0FaadFfL89qW0ZBFnnW1IUCIqEKRTR4OKmh/TPvi1Vyd6gvoHDohM8exGpmvykhGL0+n7LPtPctrrmVI/nDZS+UFrsTKEwz5mJcyraceWMzdTu/wxMWDckRxwVBx3dbQknfJXgi/WjnT8tjksOyR2Ux8EGjYW7u3hyHcNVDJ2P6Qx9TyWbR15ImSZcSq5A7NCubOw7hXCKHq9Z7YskYwOUV6rvpPZi/C737LJErFuz8vvJ7KaQ5aCFZvIMcCPVlos+XMDOdM7eQmQw23fHCdAhRLCPZxEd3QWE8lvEI+tmm+z3SS8dpDK8QEkBijQ/lrdRKaNq94qDUIcKIsci7Ag+HKLSJNZBEGi3bVTbHqzdKPtwpmCnuqLeUYeBpzqG01u3fiFr8vIT4ASfmQAeV6IP9hUUVtNfngsG0tmGzUHPoym/atMG214s/Kk1jj92ixWe9G28xbuBdljGWFwVl46GvzBGIQj86d3s9rf93FZrdMW7s4jJiEZRh5UCeg+3Siu2BrCYrq9MeLcENoYoinrgb3KKCgW0h0jfbJdIyDlodFn4AKIExF94b3TRLf42cnyu1KzuC4N9fChcdgD2tfFzPPEkyscqiQ4785QwbfaZ0DEC6IIWJbx2wJCEQ3/bHgxj1MjlQYRyBJtEz9JPrdXzLPb13fkgJY6vwTV+/+AOWghb8tJ3qcRFzr+7R5qt3kWz79srbSlskORgU1xUGZcusV8UN1PkgAVh84ZnjCY5XAxrp4jbuf24n4A9ebaNUUNdf8Sd4iTu3eUYmI8GaqnrFjfFl+wGchgX0s96ASvuv2jOrpJ8NMwQrZRRnSj/D1/KFSU3BaVpYkYwf7maceBvnVzSV0/xhxSSiUdOGp8/eCkoPSnin49obsmwmqVbTtPM5ISbSm3qjKtFcnOzGvymbHRdoBkW7gJzbHYwhVx6DugVUGwSkw+s1kaDSeXWHFwv4jhUCNS3C6WZv8B4+5yUNLhoX5rTE6HgfsGggV1z2JUadtV9vkXF2OkbdJZYNfTsgkrD2Ada4oysrR/HhiDmJre5Vq4q5IveQfSVNo3phDZcXhI7VtS3EnDJ7In6AjrnnpEiqDwbf6UVWH0fymxuT8M+W39YrxxvsgQaEP2yecld70aJxB3KMIUpBM1UaRcln8nUNpe8A7lZ27Wl7qTpOuvQ7tJtl3Krxmzr3Q9cf+3wX/+94d6yXteJTOP3DDA7+fG5TV84JcC996gkSaCdnUI/IPayl1r00apH4CQaOtJ3wW5EBcw/OIBG6ha5GSVdAW1xGTyi4ixasVbrqNBPVeYN31fg2VyykivIUrDQnvrMMzNwm8UGBnpOuxn9HLzYAWZ8UerZd7I2drV16QjwWRvCZZ4JU2vLt7/sdokyNHWXhbjuR36+dFUDJGqw4Df4oKVwfn9u188ux9pheuG1jwamfbsCgeBcae74o5RYIWtPAdQPDWkyrauucf+4nB3S4YBdgN8W0kgkgOqA+e4hprNeaW1ldOjDahMJyvkjznN1MZLHrR57NZdTx+SpZn/HMT6Io9B56XmA7T4Wr0LaKloyMUOnp/kjmD+fEYZpqqomywHWz2wg7NIlnV2Yvs4SZNFnpW1goQ70b7MBVOcpBqQO33eIth0VtALj3sNmCD2NY139iz3XBvcLM+PnekgjGsEfc4A83U7zCqhHliZo6oICoVvEh7bQWsfH9ANhppXKRulGKH3ltHN12WWQ==";
    //std::string base64 = "U2FsdGVkX198sEaJRcXaYj74/Wt1qw2tcmf5aSfr5YCNnuKz7ys3";
    std::string base64 = "U2FsdGVkX18fDwMiir2vqpWNLgbPWRSfUTF46w0Bd8DI5e4m2pOdUX";
    std::string decode = b64decode(base64.c_str(), base64.size());
    //for (int i = 0; i < decode.length(); i++) {
    //    printf("%c\n",decode[i]);
    //}
    //printf("%i\n", decode.length());
    uint32_t* decode_1 = new uint32_t[ceil(decode.length()/4)];
    for (int i = 0; i < ceil(decode.length() / 4); i++) {
        decode_1[i] = 0;
        decode_1[i] |= decode[4 * i + 3] & 0x000000ff;
        decode_1[i] |= (decode[4 * i + 2] << 8) & 0x0000ff00;
        decode_1[i] |= (decode[4 * i + 1] << 16) & 0x00ff0000;
        decode_1[i] |= (decode[4 * i] << 24) & 0xff000000;
    }

    char* w1, * w2, * w3, * w4, * w5, * w6, * w7, * w8;
    uint32_t w1_length, w2_length, w3_length, w4_length, w5_length, w6_length, w7_length, w8_length;

    readFiles(&w1_length,w1,"w1.txt");
    readFiles(&w2_length,w2,"w2.txt");
    readFiles(&w3_length,w3,"w3.txt");
    readFiles(&w4_length,w4,"w4.txt");
    readFiles(&w5_length,w5,"w5.txt");
    readFiles(&w6_length,w6,"w6.txt");
    readFiles(&w7_length,w7,"w7.txt");
    readFiles(&w8_length,w8,"w8.txt");
    //for (int j = 0; j < w1_length; j++) {
        //std::string a = (std::string)((&w1[j*5]));
        std::cout << w1 << std::endl;
    //}

    std::cout <<"Number of combinations: "<< w1_length * w2_length * w3_length * w4_length * w5_length * w6_length * w7_length * w8_length <<std::endl;

    cudaError_t cudaStatus;

    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    const int block = 128;
    const int thread = 64;
    int start = 0;
    int sharedMemorySize =1024;
    bool success = false;

    uint32_t* decode_1_dev = nullptr;
    bool * success_dev = nullptr;

    //char * a1_dev=nullptr;
    char * w1_dev=nullptr, * w2_dev = nullptr, * w3_dev = nullptr, * w4_dev = nullptr
        , * w5_dev = nullptr, * w6_dev = nullptr, * w7_dev = nullptr, * w8_dev = nullptr;
    cudaStatus = cudaMalloc((void**)&decode_1_dev, ceil(decode.length() / 4) * sizeof(uint32_t));
    cudaStatus = cudaMalloc((void**)&success_dev, sizeof(bool));

    cudaStatus = cudaMalloc((void**)&w1_dev, w1_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w2_dev, w2_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w3_dev, w3_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w4_dev, w4_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w5_dev, w5_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w6_dev, w6_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w7_dev, w7_length * 4 * sizeof(char));
    cudaStatus = cudaMalloc((void**)&w8_dev, w8_length * 4 * sizeof(char));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(decode_1_dev, decode_1, ceil(decode.length() / 4) * sizeof(uint32_t), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(success_dev, &success, sizeof(bool), cudaMemcpyHostToDevice);

    cudaStatus = cudaMemcpy(w1_dev, &w1, w1_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w2_dev, &w2, w2_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w3_dev, &w3, w3_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w4_dev, &w4, w4_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w5_dev, &w5, w5_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w6_dev, &w6, w6_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w7_dev, &w7, w7_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(w8_dev, &w8, w8_length * 4 * sizeof(char), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }


    while (true) {
        printf("id: %i\n", start);
        decrypt << <block, thread >> > (decode_1_dev, start, success_dev, 
            w1_dev, w2_dev, w3_dev, w4_dev, w5_dev, w6_dev, w7_dev, w8_dev);

        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) {
            fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
            goto Error;
        }

        cudaStatus = cudaDeviceSynchronize();
        if (cudaStatus != cudaSuccess) {
            fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
            goto Error;
        }
        cudaMemcpy(&success, success_dev, sizeof(bool), cudaMemcpyDeviceToHost);

        
        start += block * thread;
        cudaStatus = cudaMemcpy(&success, success_dev, sizeof(bool), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) {
            fprintf(stderr, "cudaMemcpy failed!");
            goto Error;
        }

        if (success) {
            printf("success\n");
            break;
        }
    }


Error:
    cudaFree(decode_1_dev);
    cudaFree(success_dev);
    cudaFree(w1_dev);
    cudaFree(w2_dev);
    cudaFree(w3_dev);
    cudaFree(w4_dev);
    cudaFree(w5_dev);
    cudaFree(w6_dev);
    cudaFree(w7_dev);
    cudaFree(w8_dev);

    return cudaStatus;
}
